import 'package:flutter_test/flutter_test.dart';

import 'package:ds_podi/ds_podi.dart';

void main() {
  test('Transformar string em máscara de telefone', () {
    expect(setPhoneMask("41987654321"), "(41) 98765-4321");
    expect(setPhoneMask("41987654321", hidden: true), "(41) XXXXX-4321");
    print(setPhoneMask("41987654321"));
    expect(setPhoneMask("5541987654321"), "+55 (41) 98765-4321");
  });
  test('Conseguir ler o arquivo json para endereço e mostrar o toString', () {
    Map<String, dynamic> json = {
      'title': 'Rua A',
      'zipcode': '8060801',
      'streetName': 'Aaaa',
      'number': '328',
      'complement': '        ',
      'neighborhood': 'Centro',
      'city': 'Curitiba',
      'state': 'Paraná',
      'addressId': 'aaaaaaaaaaaaaaaaaaaaaaaa'
    };
    final end = AddressModel.fromJson(json);
    expect(end.toString(), "Aaaa, 328, Centro, Curitiba, Paraná");
  });
}
