/// Copyright © 2020 Todos os direitos reservados ao Podi App. O podi é uma iniciativa do Grupo Tacla Shopping.
///
/// Encarregado de Dados: Podi Pagamentos Digitais LTDA | atendimento@podiapp.com.br
///
/// Utilidades diversas relacionadas ao grupo de aplicativos do Podi
library podi_utils;

export 'src/utils.dart';
