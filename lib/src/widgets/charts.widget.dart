import 'package:ds_podi/ds_podi.dart';
import 'package:flutter/material.dart';

/// Gráfico em formato de círculo, com o centro vazado.
///
/// Obrigatório passar `data`.
class DonutChart extends StatelessWidget {
  /// Tamanho que o gráfico ocupará.
  final double? height, width;

  /// O atributo `title` se refere ao texto principal que se encontrará dentro da parte vazia do gráfico.\
  /// Comumente utilizado para se referir à quantidade total de itens que está sendo analisada pelo gráfico
  ///
  /// Já o `subtitle` se refere ao texto que vai abaixo do título principal, muitas vezes utilizado para\
  /// descrever o tipo do dado analisado.
  final String? title, subtitle;

  /// Booleano responsável por mostrar ou não a legenda do gráfico. Por padrão, a legenda não será mostrada.
  final bool showLegend;

  /// Os dados que serão atribuídos ao gráfico. É necessário passar uma lista de valores para que seja possível analizar suas\
  /// porcentagens.
  ///
  /// Exemplo:
  /// ```
  /// data: [
  ///   ChartDataModel("Masculino", 12, Color(0xff48DB84)),
  ///   ChartDataModel("Feminino", 15, Color(0xff9459DF)),
  /// ];
  /// ```
  final List<ChartDataModel> data;

  /// Esse atributo determinará como será o texto dos valores de cada barra.\
  /// Determina qual texto será apresentado.
  ///
  /// ```
  /// labelText: (ChartDataModel data, _) => "${data.percentage}%"; // "10%", "20%"...
  /// ```
  final String? Function(ChartDataModel, int?)? labelText;

  /// O booleano `hasColor` determina se o gráfico terá uma cor customizada ou se será a cor padrão do sistema.\
  /// Caso `hasColor` seja verdadeiro, é necessário passar o valor de `colorFromData` ou `chartColor`.
  ///
  /// Se `colorFromData` for verdadeiro, será utilizado as cores que vêm da API no gráfico.
  final bool hasColor, colorFromData;

  /// Booleano responsável por habilitar ou não a animação de carregamento dos gráficos.\
  /// Padrão é `true`.
  final bool animate;

  /// Esse atributo é passado caso `colorFromData` for falso e `hasColor` for verdadeiro.\
  /// A cor passada nesse atributo será aplicada em todas as barras do gráfico, deixando assim uma cor padrão para todos os valores.
  final Color? chartColor;

  final DeviceType deviceType;

  final TooltipBehavior? tooltipBehavior;

  final num? Function(ChartDataModel, int)? yValueMapper;

  DonutChart({
    Key? key,
    required this.data,
    this.labelText,
    this.hasColor = false,
    this.colorFromData = false,
    this.animate = true,
    this.height,
    this.width,
    this.title,
    this.subtitle,
    this.showLegend = false,
    this.chartColor,
    this.deviceType = DeviceType.Desktop,
    this.tooltipBehavior,
    this.yValueMapper,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: Stack(children: [
        Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(children: [
              TextSpan(
                text: "${title ?? ""}\n",
                style: PodiTexts.caption.size(23),
              ),
              TextSpan(
                text: subtitle ?? "",
                style:
                    PodiTexts.caption.size(13).withColor(PodiColors.dark[300]!),
              ),
            ]),
          ),
        ),
        SfCircularChart(
          tooltipBehavior: tooltipBehavior ??
              TooltipBehavior(
                enable: data.isNotEmpty,
                format: 'point.x: point.y',
                tooltipPosition: TooltipPosition.pointer,
              ),
          legend: Legend(
            isVisible: showLegend,
            isResponsive: true,
            toggleSeriesVisibility: false,
            position: (deviceType == DeviceType.Desktop)
                ? LegendPosition.right
                : LegendPosition.bottom,
            legendItemBuilder: (name, data, point, index) {
              return Container(
                height: 30,
                width: 250,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        CircleAvatar(
                          radius: 6,
                          backgroundColor: point.color ?? chartColor,
                        ),
                        const SizedBox(width: 4),
                        SizedBox(
                          width: 150,
                          child: Text(
                            name,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: PodiTexts.caption
                                .size(11)
                                .withColor(PodiColors.neutrals[300]!),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      point.y?.toString() ?? "",
                      style: PodiTexts.caption.size(11),
                    ),
                  ],
                ),
              );
            },
          ),
          series: [
            DoughnutSeries<ChartDataModel, String>(
              animationDuration: animate ? 1000 : 0,
              radius: '100%',
              explode: true,
              explodeOffset: '10%',
              dataSource: data.isEmpty
                  ? [ChartDataModel('', 1, color: PodiColors.dark[50])]
                  : data,
              xValueMapper: (ChartDataModel data, _) => data.title,
              yValueMapper:
                  yValueMapper ?? (ChartDataModel data, _) => data.quantity,
              dataLabelMapper: labelText,
              dataLabelSettings: DataLabelSettings(
                  isVisible: labelText != null,
                  color: PodiColors.dark,
                  alignment: ChartAlignment.far),
              pointColorMapper: hasColor
                  ? (ChartDataModel data, _) =>
                      colorFromData ? data.color! : chartColor!
                  : null,
              innerRadius: '50%',
            )
          ],
        ),
      ]),
    );
  }
}

/// Gráfico de barra.
///
/// Obrigatório passar `data`.
class BarChart extends StatelessWidget {
  /// Tamanho que o gráfico ocupará.
  final double? height, width;

  /// Quanto de raio que a barra terá em suas bordas. Caso seja nulo, a barra não possuirá raio.
  final double? radius;

  /// Os dados que serão atribuídos ao gráfico. É necessário passar uma lista de valores para que seja possível analizar suas\
  /// porcentagens.
  ///
  /// Exemplo:
  /// ```
  /// data: [
  ///   ChartDataModel("Masculino", 12, Color(0xff48DB84)),
  ///   ChartDataModel("Feminino", 15, Color(0xff9459DF)),
  /// ];
  /// `
  final List<ChartDataModel> data;

  /// O booleano `hasColor` determina se o gráfico terá uma cor customizada ou se será a cor padrão do sistema.\
  /// Caso `hasColor` seja verdadeiro, é necessário passar o valor de `colorFromData` ou `chartColor`.
  ///
  /// Se `colorFromData` for verdadeiro, será utilizado as cores que vêm da API no gráfico.
  final bool hasColor, colorFromData;

  /// Booleano responsável por habilitar ou não a animação de carregamento dos gráficos.\
  /// Padrão é `true`.
  final bool animate;

  /// O valor de `isHorizontal` determinará se o gráfico será horizontal ou verical.
  final bool isHorizontal;

  /// O valor de `showPercentage` determinará se o gráfico mostrará uma porcentagem.
  final bool showPercentage;

  /// O booleano `showDomainLine` determina se será mostrado a linha que separa os valores do gráfico e a barra.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showMeasureLine` determina se será mostrado a linha que separa os valores do gráfico e a barra.\
  /// Seu valor padrão é `true`.
  final bool showDomainLine, showMeasureLine;

  /// O booleano `showDomainText` determina se será mostrado o texto dos valores no gráfico.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showMeasureText` determina se será mostrado  o texto dos valores no gráfico.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showBarText` determina se será mostrado o texto dos valores de cada barra ao seu lado.\
  /// Seu valor padrão é `true`.
  final bool showDomainText, showMeasureText, showBarValues;

  /// Esse atributo é passado caso `colorFromData` for falso e `hasColor` for verdadeiro.\
  /// A cor passada nesse atributo será aplicada em todas as barras do gráfico, deixando assim uma cor padrão para todos os valores.
  final Color? chartColor;

  final TooltipBehavior? tooltipBehavior;

  final num? Function(ChartDataModel, int)? yValueMapper;

  final String Function(ChartDataModel, int?)? labelText;

  BarChart({
    Key? key,
    required this.data,
    this.hasColor = false,
    this.showPercentage = false,
    this.colorFromData = false,
    this.isHorizontal = false,
    this.animate = true,
    this.showDomainLine = true,
    this.showMeasureLine = true,
    this.showDomainText = true,
    this.showMeasureText = true,
    this.showBarValues = true,
    this.height,
    this.width,
    this.radius,
    this.chartColor,
    this.tooltipBehavior,
    this.yValueMapper,
    this.labelText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: SfCartesianChart(
        enableAxisAnimation: animate,
        plotAreaBorderWidth: 0,
        isTransposed: isHorizontal,
        series: [
          ColumnSeries<ChartDataModel, String>(
            dataLabelMapper: labelText,
            dataLabelSettings: DataLabelSettings(
              isVisible: showBarValues,
              textStyle: TextStyle(fontSize: 10),
            ),
            dataSource: isHorizontal ? data.reversed.toList() : data,
            xValueMapper: (ChartDataModel data, _) => data.title,
            yValueMapper:
                yValueMapper ?? (ChartDataModel data, _) => data.quantity,
            pointColorMapper: hasColor
                ? (ChartDataModel data, _) =>
                    colorFromData ? data.color! : chartColor!
                : null,
            borderRadius: !isHorizontal
                ? BorderRadius.vertical(top: Radius.circular(radius ?? 8))
                : BorderRadius.horizontal(right: Radius.circular(radius ?? 8)),
          )
        ],
        tooltipBehavior: tooltipBehavior ??
            TooltipBehavior(
              enable: true,
              canShowMarker: false,
              format: 'point.x: point.y',
              header: '',
              tooltipPosition: TooltipPosition.pointer,
            ),
        primaryXAxis: CategoryAxis(
          majorTickLines: const MajorTickLines(width: 0),
          majorGridLines: showDomainLine
              ? const MajorGridLines(width: 0.3)
              : const MajorGridLines(width: 0),
          axisLine: const AxisLine(width: 0),
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: showDomainText,
        ),
        primaryYAxis: NumericAxis(
          majorTickLines: const MajorTickLines(size: 0),
          majorGridLines: showMeasureLine
              ? const MajorGridLines(width: 0.3)
              : const MajorGridLines(width: 0),
          axisLine: const AxisLine(width: 0),
          labelFormat: '{value}${showPercentage ? '%' : ''}',
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: showMeasureText,
        ),
      ),
    );
  }
}

/// Gráfico de barra em grupo.
///
/// Obrigatório passar `data`.
class GroupedBarChart extends StatelessWidget {
  /// Tamanho que o gráfico ocupará.
  final double? height, width;

  /// Quanto de raio que a barra terá em suas bordas. Caso seja nulo, a barra não possuirá raio.
  final double? radius;

  /// Os dados que serão atribuídos ao gráfico. É necessário passar uma lista com listas de valores para que seja possível analizar suas\
  /// porcentagens.
  ///
  /// Exemplo:
  /// ```
  /// data: [
  ///   // Primeiro valor do gráfico
  ///   [
  ///     ChartDataModel("Jan", 12, Color(0xff48DB84)),
  ///     ChartDataModel("Fev", 15, Color(0xff48DB84)),
  ///   ],
  ///   // Segundo valor do gráfico
  ///   [
  ///     ChartDataModel("Jan", 17, Color(0xff9459DF)),
  ///     ChartDataModel("Fev", 10, Color(0xff9459DF)),
  ///   ]
  /// ];
  /// ```
  final List<ChartDataModel> data;

  /// O booleano `hasColor` determina se o gráfico terá uma cor customizada ou se será a cor padrão do sistema.\
  /// Caso `hasColor` seja verdadeiro, é necessário passar o valor de `colorFromData` ou `chartColor`.
  ///
  /// Se `colorFromData` for verdadeiro, será utilizado as cores que vêm da API no gráfico.
  final bool hasColor, colorFromData;

  /// Booleano responsável por habilitar ou não a animação de carregamento dos gráficos.\
  /// Padrão é `true`.
  final bool animate;

  /// O valor de `isHorizontal` determinará se o gráfico será horizontal ou verical.
  final bool isHorizontal;

  /// O valor de `showPercentage` determinará se o gráfico mostrará uma porcentagem.
  final bool showPercentage;

  /// O booleano `showDomainLine` determina se será mostrado a linha que separa os valores do gráfico e a barra.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showMeasureLine` determina se será mostrado a linha que separa os valores do gráfico e a barra.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showInsideLine` determina se será mostrado a linha que mostra quais valores das barras correspondem aos valores do gráfico.\
  /// Seu valor padrão é `true`.
  final bool showDomainLine, showMeasureLine, showInsideLine;

  /// O booleano `showDomainText` determina se será mostrado o texto dos valores no gráfico.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showMeasureText` determina se será mostrado  o texto dos valores no gráfico.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showBarText` determina se será mostrado o texto dos valores de cada barra ao seu lado.\
  /// Seu valor padrão é `true`.
  final bool showDomainText, showMeasureText, showBarValues;

  /// Esse atributo é passado caso `colorFromData` for falso e `hasColor` for verdadeiro.\
  /// A cor passada nesse atributo será aplicada em todas as barras do gráfico, deixando assim uma cor padrão para todos os valores.
  final Color? chartColor;

  /// Esse atributo determinará como será o texto dos valores de cada barra.\
  /// Determina qual texto será apresentado.
  ///
  /// ```
  /// labelText: (ChartDataModel data, _) => "${data.percentage}%"; // "10%", "20%"...
  /// ```
  final String Function(ChartDataModel, int?)? labelText;

  final TooltipBehavior? tooltipBehavior;

  final num? Function(ChartDataModel, int)? yValueMapper;

  GroupedBarChart({
    Key? key,
    required this.data,
    this.hasColor = false,
    this.showPercentage = false,
    this.colorFromData = false,
    this.isHorizontal = false,
    this.animate = true,
    this.showDomainLine = true,
    this.showMeasureLine = true,
    this.showInsideLine = true,
    this.showDomainText = true,
    this.showMeasureText = true,
    this.showBarValues = true,
    this.labelText,
    this.height,
    this.width,
    this.radius,
    this.chartColor,
    this.tooltipBehavior,
    this.yValueMapper,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: SfCartesianChart(
        enableAxisAnimation: animate,
        plotAreaBorderWidth: 0,
        isTransposed: isHorizontal,
        series: [
          ColumnSeries<ChartDataModel, String>(
            spacing: 0.2,
            dataSource: isHorizontal ? data.reversed.toList() : data,
            xValueMapper: (ChartDataModel data, _) => data.title,
            yValueMapper:
                yValueMapper ?? (ChartDataModel data, _) => data.quantity,
            pointColorMapper: hasColor
                ? (ChartDataModel data, _) =>
                    colorFromData ? data.color! : chartColor!
                : (ChartDataModel data, _) => PodiColors.green,
            dataLabelSettings: DataLabelSettings(
                isVisible: showBarValues, textStyle: TextStyle(fontSize: 10)),
            borderRadius: !isHorizontal
                ? BorderRadius.vertical(top: Radius.circular(radius ?? 8))
                : BorderRadius.horizontal(right: Radius.circular(radius ?? 8)),
          ),
          ColumnSeries<ChartDataModel, String>(
            spacing: 0.2,
            dataSource: isHorizontal ? data.reversed.toList() : data,
            xValueMapper: (ChartDataModel data, _) => data.title,
            yValueMapper:
                yValueMapper ?? (ChartDataModel data, _) => data.otherValue,
            pointColorMapper: hasColor
                ? (ChartDataModel data, _) =>
                    colorFromData ? data.color! : chartColor!
                : (ChartDataModel data, _) => PodiColors.purple,
            dataLabelSettings: DataLabelSettings(
                isVisible: showBarValues, textStyle: TextStyle(fontSize: 10)),
            borderRadius: !isHorizontal
                ? BorderRadius.vertical(top: Radius.circular(radius ?? 8))
                : BorderRadius.horizontal(right: Radius.circular(radius ?? 8)),
          ),
        ],
        tooltipBehavior: tooltipBehavior ??
            TooltipBehavior(
              enable: true,
              canShowMarker: false,
              format: 'point.x: point.y',
              header: '',
              tooltipPosition: TooltipPosition.pointer,
            ),
        primaryXAxis: CategoryAxis(
          majorTickLines: const MajorTickLines(width: 0),
          majorGridLines: showDomainLine
              ? const MajorGridLines(width: 0.3)
              : const MajorGridLines(width: 0),
          axisLine: const AxisLine(width: 0),
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: showDomainText,
        ),
        primaryYAxis: NumericAxis(
          majorTickLines: const MajorTickLines(size: 0),
          majorGridLines: showMeasureLine
              ? const MajorGridLines(width: 0.3)
              : const MajorGridLines(width: 0),
          axisLine: const AxisLine(width: 0),
          labelFormat: '{value}${showPercentage ? '%' : ''}',
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: showMeasureText,
        ),
      ),
    );
  }
}

/// Gráfico de pizza.
///
/// Obrigatório passar `data`.
class PieChart extends StatelessWidget {
  /// Tamanho que o gráfico ocupará.
  final double? height, width;

  /// Os dados que serão atribuídos ao gráfico. É necessário passar uma lista de valores para que seja possível analizar suas\
  /// porcentagens.
  ///
  /// Exemplo:
  /// ```
  /// data: [
  ///   ChartDataModel("Masculino", 12, Color(0xff48DB84)),
  ///   ChartDataModel("Feminino", 15, Color(0xff9459DF)),
  /// ];
  /// ```
  final List<ChartDataModel> data;

  /// Esse atributo determinará como será o texto dos valores de cada barra.\
  /// Determina qual texto será apresentado.
  ///
  /// ```
  /// labelText: (ChartDataModel data, _) => "${data.percentage}%"; // "10%", "20%"...
  /// ```
  final String? Function(ChartDataModel, int?)? labelText;

  /// O booleano `hasColor` determina se o gráfico terá uma cor customizada ou se será a cor padrão do sistema.\
  /// Caso `hasColor` seja verdadeiro, é necessário passar o valor de `colorFromData` ou `chartColor`.
  ///
  /// Se `colorFromData` for verdadeiro, será utilizado as cores que vêm da API no gráfico.
  final bool hasColor, colorFromData;

  /// Booleano responsável por mostrar ou não a legenda do gráfico. Por padrão, a legenda não será mostrada.
  final bool showLegend;

  /// Booleano responsável por habilitar ou não a animação de carregamento dos gráficos.\
  /// Padrão é `true`.
  final bool animate;

  /// Esse atributo é passado caso `colorFromData` for falso e `hasColor` for verdadeiro.\
  /// A cor passada nesse atributo será aplicada em todas as barras do gráfico, deixando assim uma cor padrão para todos os valores.
  final Color? chartColor;

  final TooltipBehavior? tooltipBehavior;

  final num? Function(ChartDataModel, int)? yValueMapper;

  PieChart({
    Key? key,
    required this.data,
    this.labelText,
    this.hasColor = false,
    this.colorFromData = false,
    this.animate = true,
    this.showLegend = false,
    this.height,
    this.width,
    this.chartColor,
    this.tooltipBehavior,
    this.yValueMapper,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: SfCircularChart(
        tooltipBehavior: tooltipBehavior ??
            TooltipBehavior(
              enable: data.isNotEmpty,
              format: 'point.x: point.y',
              tooltipPosition: TooltipPosition.pointer,
            ),
        legend: Legend(
          isVisible: showLegend,
          isResponsive: true,
          toggleSeriesVisibility: false,
          position: LegendPosition.right,
          legendItemBuilder: (name, data, point, index) {
            return Container(
              height: 30,
              width: 250,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CircleAvatar(
                        radius: 6,
                        backgroundColor: point.color ?? chartColor,
                      ),
                      const SizedBox(width: 4),
                      SizedBox(
                        width: 150,
                        child: Text(
                          name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: PodiTexts.caption
                              .size(11)
                              .withColor(PodiColors.neutrals[300]!),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    point.y?.toString() ?? "",
                    style: PodiTexts.caption.size(11),
                  ),
                ],
              ),
            );
          },
        ),
        series: [
          PieSeries<ChartDataModel, String>(
            animationDuration: animate ? 1000 : 0,
            radius: '100%',
            explode: true,
            explodeOffset: '10%',
            dataSource: data.isEmpty
                ? [ChartDataModel('', 1, color: PodiColors.dark[50])]
                : data,
            xValueMapper: (ChartDataModel data, _) => data.title,
            yValueMapper:
                yValueMapper ?? (ChartDataModel data, _) => data.quantity,
            dataLabelMapper: labelText,
            dataLabelSettings: DataLabelSettings(
                isVisible: labelText != null,
                color: PodiColors.dark,
                alignment: ChartAlignment.far),
            pointColorMapper: hasColor
                ? (ChartDataModel data, _) =>
                    colorFromData ? data.color! : chartColor!
                : null,
          )
        ],
      ),
    );
  }
}

/// Gráfico de linha.
///
/// Obrigatório passar `data`.
class LineChart extends StatelessWidget {
  /// Tamanho que o gráfico ocupará.
  final double? height, width;

  /// Os dados que serão atribuídos ao gráfico. É necessário passar uma lista de valores para que seja possível analizar suas\
  /// porcentagens.
  ///
  /// Exemplo:
  /// ```
  /// data: [
  ///   [
  ///    ChartDataModel("2021", 1, otherValue: 100),
  ///    ChartDataModel("2021", 2, otherValue: 200),
  ///   ],
  ///   [
  ///    ChartDataModel("2022", 1, otherValue: 300),
  ///    ChartDataModel("2022", 2, otherValue: 400),
  ///   ],
  /// ];
  /// ```
  final List<List<ChartDataModel>> data;

  /// Esse atributo determinará como será o texto dos valores de cada barra.\
  /// Determina qual texto será apresentado.
  ///
  /// ```
  /// labelText: (ChartDataModel data, _) => "${data.percentage}%"; // "10%", "20%"...
  /// ```
  final String? Function(ChartDataModel, int?)? labelText;

  /// O booleano `hasColor` determina se o gráfico terá uma cor customizada ou se será a cor padrão do sistema.\
  /// Caso `hasColor` seja verdadeiro, é necessário passar o valor de `colorFromData` ou `chartColor`.
  ///
  /// Se `colorFromData` for verdadeiro, será utilizado as cores que vêm da API no gráfico.
  final bool hasColor, colorFromData;

  /// O booleano `showDomainLine` determina se será mostrado a linha que separa os valores do gráfico e a barra.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showMeasureLine` determina se será mostrado a linha que separa os valores do gráfico e a barra.\
  /// Seu valor padrão é `true`.
  final bool showDomainLine, showMeasureLine;

  /// O booleano `showDomainText` determina se será mostrado o texto dos valores no gráfico.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showMeasureText` determina se será mostrado  o texto dos valores no gráfico.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showBarText` determina se será mostrado o texto dos valores de cada barra ao seu lado.\
  /// Seu valor padrão é `true`.
  final bool showDomainText, showMeasureText;

  /// Booleano responsável por habilitar ou não a animação de carregamento dos gráficos.\
  /// Padrão é `true`.
  final bool animate;

  /// Esse atributo é passado caso `colorFromData` for falso e `hasColor` for verdadeiro.\
  /// A cor passada nesse atributo será aplicada em todas as barras do gráfico, deixando assim uma cor padrão para todos os valores.
  final Color? chartColor;

  /// O valor de `showPercentage` determinará se o gráfico mostrará uma porcentagem.
  final bool showPercentage;

  final num? Function(ChartDataModel, int)? yValueMapper;

  LineChart({
    Key? key,
    required this.data,
    this.labelText,
    this.showPercentage = false,
    this.hasColor = false,
    this.colorFromData = false,
    this.animate = true,
    this.showDomainLine = true,
    this.showMeasureLine = true,
    this.showDomainText = true,
    this.showMeasureText = true,
    this.height,
    this.width,
    this.chartColor,
    this.yValueMapper,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: SfCartesianChart(
        enableAxisAnimation: animate,
        plotAreaBorderWidth: 0,
        tooltipBehavior: TooltipBehavior(
          enable: data.isNotEmpty,
          format: 'point.x: point.y',
          header: '',
          tooltipPosition: TooltipPosition.pointer,
        ),
        series: List.generate(
            data.length,
            (index) => LineSeries<ChartDataModel, dynamic>(
                  animationDuration: animate ? 1000 : 0,
                  dataSource: data[index].isEmpty
                      ? [ChartDataModel('', 1, color: PodiColors.dark[50])]
                      : data[index],
                  xValueMapper: (ChartDataModel data, _) => data.quantity,
                  yValueMapper: yValueMapper ??
                      (ChartDataModel data, _) => data.otherValue,
                  dataLabelMapper: labelText,
                  dataLabelSettings: DataLabelSettings(
                      isVisible: labelText != null,
                      color: PodiColors.dark,
                      alignment: ChartAlignment.far),
                  pointColorMapper: hasColor
                      ? (ChartDataModel data, _) =>
                          colorFromData ? data.color! : chartColor!
                      : null,
                )),
        primaryXAxis: CategoryAxis(
          majorTickLines: const MajorTickLines(width: 0),
          majorGridLines: showDomainLine
              ? const MajorGridLines(width: 0.3)
              : const MajorGridLines(width: 0),
          axisLine: const AxisLine(width: 0),
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: showDomainText,
        ),
        primaryYAxis: NumericAxis(
          majorTickLines: const MajorTickLines(size: 0),
          majorGridLines: showMeasureLine
              ? const MajorGridLines(width: 0.3)
              : const MajorGridLines(width: 0),
          axisLine: const AxisLine(width: 0),
          labelFormat: '{value}${showPercentage ? '%' : ''}',
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: showMeasureText,
        ),
      ),
    );
  }
}

/// Gráfico de linha com área.
///
/// Obrigatório passar `data`.
class AreaChart extends StatelessWidget {
  /// Tamanho que o gráfico ocupará.
  final double? height, width;

  /// Os dados que serão atribuídos ao gráfico. É necessário passar uma lista de valores para que seja possível analizar suas\
  /// porcentagens.
  ///
  /// Exemplo:
  /// ```
  /// data: [
  ///   [
  ///    ChartDataModel("2021", 1, otherValue: 100),
  ///    ChartDataModel("2021", 2, otherValue: 200),
  ///   ],
  ///   [
  ///    ChartDataModel("2022", 1, otherValue: 300),
  ///    ChartDataModel("2022", 2, otherValue: 400),
  ///   ],
  /// ];
  /// ```
  final List<ChartDataModel> data;

  /// Esse atributo determinará como será o texto dos valores de cada barra.\
  /// Determina qual texto será apresentado.
  ///
  /// ```
  /// labelText: (ChartDataModel data, _) => "${data.percentage}%"; // "10%", "20%"...
  /// ```
  final String? Function(ChartDataModel, int?)? labelText;

  /// O booleano `showDomainLine` determina se será mostrado a linha que separa os valores do gráfico e a barra.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showMeasureLine` determina se será mostrado a linha que separa os valores do gráfico e a barra.\
  /// Seu valor padrão é `true`.
  final bool showDomainLine, showMeasureLine;

  /// O booleano `showDomainText` determina se será mostrado o texto dos valores no gráfico.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showMeasureText` determina se será mostrado  o texto dos valores no gráfico.\
  /// Seu valor padrão é `true`.
  ///
  /// O booleano `showBarText` determina se será mostrado o texto dos valores de cada barra ao seu lado.\
  /// Seu valor padrão é `true`.
  final bool showDomainText, showMeasureText;

  /// Booleano responsável por habilitar ou não a animação de carregamento dos gráficos.\
  /// Padrão é `true`.
  final bool animate;

  /// Esse atributo é passado caso `colorFromData` for falso e `hasColor` for verdadeiro.\
  /// A cor passada nesse atributo será aplicada em todas as barras do gráfico, deixando assim uma cor padrão para todos os valores.
  final Color? chartColor;

  /// O valor de `showPercentage` determinará se o gráfico mostrará uma porcentagem.
  final bool showPercentage;

  final TooltipBehavior? tooltipBehavior;

  final num? Function(ChartDataModel, int)? yValueMapper;

  AreaChart({
    Key? key,
    required this.data,
    this.labelText,
    this.showPercentage = false,
    this.animate = true,
    this.showDomainLine = true,
    this.showMeasureLine = true,
    this.showDomainText = true,
    this.showMeasureText = true,
    this.height,
    this.width,
    this.chartColor,
    this.tooltipBehavior,
    this.yValueMapper,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: SfCartesianChart(
        tooltipBehavior: tooltipBehavior,
        enableAxisAnimation: animate,
        plotAreaBorderWidth: 0,
        series: List.generate(
            data.length,
            (index) => AreaSeries<ChartDataModel, String>(
                  borderColor: PodiColors.green,
                  borderWidth: 2,
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      PodiColors.green,
                      PodiColors.white,
                    ],
                  ),
                  animationDuration: animate ? 1000 : 0,
                  dataSource: data,
                  xValueMapper: (ChartDataModel data, _) => data.title,
                  yValueMapper:
                      yValueMapper ?? (ChartDataModel data, _) => data.quantity,
                  markerSettings: const MarkerSettings(
                    isVisible: true,
                    color: PodiColors.green,
                    borderColor: PodiColors.white,
                    borderWidth: 2,
                    height: 12,
                    width: 12,
                  ),
                  dataLabelMapper: labelText,
                  dataLabelSettings: DataLabelSettings(
                      isVisible: labelText != null,
                      color: PodiColors.neutrals[800]!,
                      alignment: ChartAlignment.far),
                )),
        primaryXAxis: CategoryAxis(
          labelPlacement: LabelPlacement.onTicks,
          majorTickLines: const MajorTickLines(width: 0),
          majorGridLines: showDomainLine
              ? const MajorGridLines(width: 0.3)
              : const MajorGridLines(width: 0),
          axisLine: const AxisLine(width: 0),
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: showDomainText,
        ),
        primaryYAxis: NumericAxis(
          majorTickLines: const MajorTickLines(size: 0),
          majorGridLines: showMeasureLine
              ? const MajorGridLines(width: 0.3)
              : const MajorGridLines(width: 0),
          axisLine: const AxisLine(width: 0),
          labelFormat: '{value}${showPercentage ? '%' : ''}',
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: showMeasureText,
        ),
      ),
    );
  }
}
