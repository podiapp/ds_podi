import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';

import '../utils.dart';

export 'package:html_editor_enhanced/html_editor.dart' hide NotificationType;

class HtmlTextField extends StatelessWidget {
  final HtmlEditorController controller;
  final BorderRadiusGeometry? borderRadius;
  final bool showToolbar;
  final Function(String?)? onChanged;
  final Function()? onFocus;
  final double? height;
  final String? initialText;
  final String? hint;

  const HtmlTextField({
    required this.controller,
    this.onChanged,
    this.onFocus,
    this.showToolbar = true,
    this.height,
    this.initialText,
    this.borderRadius,
    this.hint = "",
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      decoration: BoxDecoration(
        border: Border.all(color: PodiColors.dark[100]!),
        borderRadius: borderRadius ??
            const BorderRadius.vertical(top: Radius.circular(4)),
      ),
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (showToolbar) ...[
            ToolbarWidget(
              controller: controller,
              htmlToolbarOptions: HtmlToolbarOptions(
                toolbarType: ToolbarType.nativeGrid,
                toolbarPosition: ToolbarPosition.custom,
                textStyle: PodiTexts.caption.size(13),
                separatorWidget: const SizedBox(width: 8),
                defaultToolbarButtons: [
                  const FontButtons(
                    subscript: false,
                    superscript: false,
                  ),
                  const ListButtons(listStyles: false),
                  const OtherButtons(
                    fullscreen: false,
                    codeview: false,
                    help: false,
                    copy: false,
                    paste: false,
                  ),
                ],
              ),
              callbacks: Callbacks(),
            ),
          ],
          Expanded(
            flex: height != null ? 1 : 0,
            child: HtmlEditor(
              callbacks: Callbacks(
                onInit: () => controller.setFullScreen(),
                onFocus: onFocus,
                onChangeContent: onChanged,
              ),
              controller: controller,
              htmlEditorOptions: HtmlEditorOptions(
                darkMode: false,
                initialText: initialText,
                hint: hint ?? "",
                autoAdjustHeight: true,
              ),
              htmlToolbarOptions: const HtmlToolbarOptions(
                toolbarType: ToolbarType.nativeGrid,
                toolbarPosition: ToolbarPosition.custom,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
