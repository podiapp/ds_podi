import 'package:ds_podi/ds_podi.dart';
import 'package:flutter/material.dart';

class PodiCharts<T> {
  final List<PodiChartData<T>> data;
  final PodiChartSettings<T>? settings;
  final PodiChartStyle? style;
  final DeviceType device;

  PodiCharts({
    required this.data,
    this.style,
    this.settings,
    this.device = DeviceType.Desktop,
  }) {
    _style = this.style ?? PodiChartStyle();
    _settings = this.settings ?? PodiChartSettings<T>();
  }

  late PodiChartStyle _style;
  late PodiChartSettings<T> _settings;

  List<Color> get _palette {
    return [
      PodiColors.green,
      PodiColors.purple,
      PodiColors.warning,
      PodiColors.danger,
      PodiColors.discounts,
      PodiColors.blue,
      PodiColors.green[300]!,
      PodiColors.danger[400]!,
      PodiColors.purple[300]!,
      PodiColors.warning[400]!,
      PodiColors.discounts[100]!,
      PodiColors.blue[500]!,
    ];
  }

  TooltipBehavior get _tooltipBehavior {
    return TooltipBehavior(
      tooltipPosition: TooltipPosition.pointer,
      enable: _style.enableTooltip,
      format: _style.tooltipFormat,
      canShowMarker: false,
      header: "",
      duration: 1,
    );
  }

  Legend get _legend {
    return Legend(
      isVisible: _style.enableLegend,
      position: (device == DeviceType.Desktop)
          ? LegendPosition.right
          : LegendPosition.bottom,
      legendItemBuilder: _style.legendItemBuilder ??
          (name, data, point, index) {
            return Container(
              height: 30,
              width: 200,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CircleAvatar(
                        radius: 6,
                        backgroundColor: point.color ?? _palette[index],
                      ),
                      const SizedBox(width: 4),
                      SizedBox(
                        width: 150,
                        child: Text(
                          name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: PodiTexts.caption
                              .size(11)
                              .withColor(PodiColors.neutrals[300]!),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    (point.y ?? 0).toString(),
                    style: PodiTexts.caption.size(11),
                  ),
                ],
              ),
            );
          },
    );
  }

  Widget get donut {
    return SizedBox(
      height: _style.height,
      width: _style.width,
      child: SfCircularChart(
        tooltipBehavior: _tooltipBehavior,
        legend: _legend,
        palette: _palette,
        series: [
          DoughnutSeries<PodiChartData<T>, T>(
            dataSource: data,
            explode: true,
            radius: '100%',
            explodeOffset: '10%',
            innerRadius: '50%',
            animationDuration:
                _style.enableAnimation ? _style.animationDuration : 0,
            dataLabelSettings: DataLabelSettings(
              isVisible: _style.enableDataLabel,
              color: PodiColors.neutrals[900],
              alignment: ChartAlignment.far,
            ),
            pointColorMapper: (data, _) => data.color,
            xValueMapper: _settings.xValue ?? (data, _) => data.x,
            yValueMapper: _settings.yValue ?? (data, _) => data.y,
            dataLabelMapper: _settings.dataLabelValue,
          ),
        ],
      ),
    );
  }

  Widget get pie {
    return SizedBox(
      height: _style.height,
      width: _style.width,
      child: SfCircularChart(
        tooltipBehavior: _tooltipBehavior,
        legend: _legend,
        palette: _palette,
        series: [
          PieSeries<PodiChartData<T>, T>(
            dataSource: data,
            explode: true,
            radius: '100%',
            explodeOffset: '10%',
            animationDuration:
                _style.enableAnimation ? _style.animationDuration : 0,
            dataLabelSettings: DataLabelSettings(
              isVisible: _style.enableDataLabel,
              color: PodiColors.neutrals[900],
              alignment: ChartAlignment.far,
            ),
            pointColorMapper: (data, _) => data.color,
            xValueMapper: _settings.xValue ?? (data, _) => data.x,
            yValueMapper: _settings.yValue ?? (data, _) => data.y,
            dataLabelMapper: _settings.dataLabelValue,
          ),
        ],
      ),
    );
  }

  Widget get bar {
    return SizedBox(
      height: _style.height,
      width: _style.width,
      child: SfCartesianChart(
        enableSideBySideSeriesPlacement: _style.showSideBySide,
        enableAxisAnimation: _style.enableAnimation,
        tooltipBehavior: _tooltipBehavior,
        legend: _legend,
        isTransposed: (_settings.chartAxis == Axis.horizontal),
        plotAreaBorderWidth: 0,
        palette: _palette,
        series: [
          ColumnSeries<PodiChartData<T>, T>(
            isTrackVisible: _style.showTrack,
            trackColor: PodiColors.neutrals[100]!.withOpacity(0.3),
            dataSource: (_settings.chartAxis == Axis.horizontal)
                ? data.reversed.toList()
                : data,
            animationDuration:
                _style.enableAnimation ? _style.animationDuration : 0,
            dataLabelSettings: DataLabelSettings(
              isVisible: _style.enableDataLabel,
              color: PodiColors.neutrals[900],
              alignment: ChartAlignment.far,
            ),
            pointColorMapper: (data, _) => data.color,
            xValueMapper: _settings.xValue ?? (data, _) => data.x,
            yValueMapper: _settings.yValue ?? (data, _) => data.y,
            dataLabelMapper: _settings.dataLabelValue,
            borderRadius: (_settings.chartAxis == Axis.horizontal)
                ? BorderRadius.horizontal(
                    right: Radius.circular(_style.barRadius))
                : BorderRadius.vertical(top: Radius.circular(_style.barRadius)),
          ),
        ],
        primaryXAxis: CategoryAxis(
          majorTickLines: const MajorTickLines(width: 0),
          majorGridLines: MajorGridLines(width: _style.showYLines ? 0.3 : 0),
          axisLine: const AxisLine(width: 0),
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: _style.showYValues,
        ),
        primaryYAxis: NumericAxis(
          majorTickLines: const MajorTickLines(size: 0),
          majorGridLines: MajorGridLines(width: _style.showXLines ? 0.3 : 0),
          axisLine: const AxisLine(width: 0),
          labelFormat: _settings.yLabel,
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: _style.showXValues,
        ),
      ),
    );
  }

  Widget get groupedBar {
    return SizedBox(
      height: _style.height,
      width: _style.width,
      child: SfCartesianChart(
        enableAxisAnimation: _style.enableAnimation,
        tooltipBehavior: _tooltipBehavior,
        legend: _legend,
        isTransposed: (_settings.chartAxis == Axis.horizontal),
        plotAreaBorderWidth: 0,
        palette: _palette,
        series: List<ColumnSeries<PodiChartData<T>, T>>.generate(
          data.length,
          (index) {
            final item = data[index];
            return ColumnSeries<PodiChartData<T>, T>(
              dataSource: (_settings.chartAxis == Axis.horizontal)
                  ? item.group.reversed.toList()
                  : item.group,
              animationDuration:
                  _style.enableAnimation ? _style.animationDuration : 0,
              dataLabelSettings: DataLabelSettings(
                isVisible: _style.enableDataLabel,
                color: PodiColors.neutrals[900],
                alignment: ChartAlignment.far,
              ),
              pointColorMapper: (data, _) => data.color,
              xValueMapper: _settings.xValue ?? (data, _) => data.x,
              yValueMapper: _settings.yValue ?? (data, _) => data.y,
              dataLabelMapper: _settings.dataLabelValue,
              borderRadius: (_settings.chartAxis == Axis.horizontal)
                  ? BorderRadius.horizontal(
                      right: Radius.circular(_style.barRadius))
                  : BorderRadius.vertical(
                      top: Radius.circular(_style.barRadius)),
            );
          },
        ),
        primaryXAxis: CategoryAxis(
          majorTickLines: const MajorTickLines(width: 0),
          majorGridLines: MajorGridLines(width: _style.showYLines ? 0.3 : 0),
          axisLine: const AxisLine(width: 0),
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: _style.showYValues,
        ),
        primaryYAxis: NumericAxis(
          majorTickLines: const MajorTickLines(size: 0),
          majorGridLines: MajorGridLines(width: _style.showXLines ? 0.3 : 0),
          axisLine: const AxisLine(width: 0),
          labelFormat: _settings.yLabel,
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: _style.showXValues,
        ),
      ),
    );
  }

  Widget get line {
    return SizedBox(
      height: _style.height,
      width: _style.width,
      child: SfCartesianChart(
        enableAxisAnimation: _style.enableAnimation,
        tooltipBehavior: _tooltipBehavior,
        legend: _legend,
        isTransposed: (_settings.chartAxis == Axis.horizontal),
        plotAreaBorderWidth: 0,
        palette: _palette,
        series: List<LineSeries<PodiChartData<T>, T>>.generate(
          data.length,
          (index) {
            final item = data[index];
            return LineSeries<PodiChartData<T>, T>(
              dataSource: (_settings.chartAxis == Axis.horizontal)
                  ? item.group.reversed.toList()
                  : item.group,
              animationDuration:
                  _style.enableAnimation ? _style.animationDuration : 0,
              dataLabelSettings: DataLabelSettings(
                isVisible: _style.enableDataLabel,
                color: PodiColors.neutrals[900],
                alignment: ChartAlignment.near,
              ),
              pointColorMapper: (data, _) => data.color,
              xValueMapper: _settings.xValue ?? (data, _) => data.x,
              yValueMapper: _settings.yValue ?? (data, _) => data.y,
              dataLabelMapper: _settings.dataLabelValue,
            );
          },
        ),
        primaryXAxis: CategoryAxis(
          majorTickLines: const MajorTickLines(width: 0),
          majorGridLines: MajorGridLines(width: _style.showYLines ? 0.3 : 0),
          axisLine: const AxisLine(width: 0),
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: _style.showYValues,
        ),
        primaryYAxis: NumericAxis(
          majorTickLines: const MajorTickLines(size: 0),
          majorGridLines: MajorGridLines(width: _style.showXLines ? 0.3 : 0),
          axisLine: const AxisLine(width: 0),
          labelFormat: _settings.yLabel,
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: _style.showXValues,
        ),
      ),
    );
  }

  Widget get area {
    return SizedBox(
      height: _style.height,
      width: _style.width,
      child: SfCartesianChart(
        enableAxisAnimation: _style.enableAnimation,
        tooltipBehavior: _tooltipBehavior,
        legend: _legend,
        isTransposed: (_settings.chartAxis == Axis.horizontal),
        plotAreaBorderWidth: 0,
        palette: _palette,
        series: List<AreaSeries<PodiChartData<T>, T>>.generate(
          data.length,
          (index) {
            return AreaSeries<PodiChartData<T>, T>(
              borderColor: _style.borderColor ?? PodiColors.green,
              borderWidth: 2,
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  _style.fillColor ?? PodiColors.green,
                  PodiColors.white,
                ],
              ),
              dataSource: (_settings.chartAxis == Axis.horizontal)
                  ? data.reversed.toList()
                  : data,
              animationDuration:
                  _style.enableAnimation ? _style.animationDuration : 0,
              dataLabelSettings: DataLabelSettings(
                isVisible: _style.enableDataLabel,
                alignment: ChartAlignment.near,
                builder: (data, point, series, pointIndex, seriesIndex) {
                  return Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: PodiColors.neutrals[900],
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 4,
                      vertical: 2,
                    ),
                    child: Text(
                      data.y.toString(),
                      style: PodiTexts.caption
                          .size(11)
                          .withColor(PodiColors.white),
                    ),
                  );
                },
              ),
              markerSettings: MarkerSettings(
                isVisible: _style.enableMarker,
                color: _style.markerColor ?? PodiColors.green,
                borderColor: PodiColors.white,
                borderWidth: 2,
                height: 12,
                width: 12,
              ),
              pointColorMapper: (data, _) => data.color,
              xValueMapper: _settings.xValue ?? (data, _) => data.x,
              yValueMapper: _settings.yValue ?? (data, _) => data.y,
              dataLabelMapper: _settings.dataLabelValue,
            );
          },
        ),
        primaryXAxis: CategoryAxis(
          majorTickLines: const MajorTickLines(width: 0),
          majorGridLines: MajorGridLines(width: _style.showYLines ? 0.3 : 0),
          axisLine: const AxisLine(width: 0),
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: _style.showYValues,
        ),
        primaryYAxis: NumericAxis(
          majorTickLines: const MajorTickLines(size: 0),
          majorGridLines: MajorGridLines(width: _style.showXLines ? 0.3 : 0),
          axisLine: const AxisLine(width: 0),
          labelFormat: _settings.yLabel,
          labelStyle: PodiTexts.theme(
            fontWeight: FontWeight.w500,
            fontSize: 11,
            height: 1.2,
            color: PodiColors.neutrals[300],
          ),
          isVisible: _style.showXValues,
        ),
      ),
    );
  }
}

class PodiChartData<T> {
  final T x;
  final num y;
  final double? percentage;
  final Color? color;
  final List<PodiChartData<T>> group;

  PodiChartData({
    required this.x,
    required this.y,
    this.percentage,
    this.color,
    this.group = const [],
  });
}

class PodiChartSettings<T> {
  final T? Function(PodiChartData<T> data, int index)? xValue;
  final num? Function(PodiChartData<T> data, int index)? yValue;
  final String? Function(PodiChartData<T> data, int index)? dataLabelValue;
  final Axis chartAxis;
  final String yLabel;

  PodiChartSettings({
    this.xValue,
    this.yValue,
    this.dataLabelValue,
    this.chartAxis = Axis.vertical,
    this.yLabel = "{value}",
  });
}

class PodiChartStyle {
  double? height, width;
  bool enableTooltip,
      enableLegend,
      showSideBySide,
      enableAnimation,
      enableDataLabel,
      showTrack,
      enableMarker;
  bool showYLines, showYValues, showXLines, showXValues;
  double animationDuration;
  double barRadius;
  String tooltipFormat;
  Color? markerColor, fillColor, borderColor;
  Widget Function(
    String name,
    dynamic data,
    dynamic point,
    int index,
  )? legendItemBuilder;

  PodiChartStyle({
    this.height,
    this.width,
    this.enableTooltip = true,
    this.enableLegend = true,
    this.enableAnimation = true,
    this.enableDataLabel = true,
    this.enableMarker = true,
    this.showSideBySide = true,
    this.showTrack = false,
    this.showYLines = true,
    this.showXLines = true,
    this.showYValues = true,
    this.showXValues = true,
    this.animationDuration = 1000,
    this.barRadius = 8,
    this.tooltipFormat = "point.x: point.y",
    this.legendItemBuilder,
    this.markerColor,
    this.fillColor,
    this.borderColor,
  });
}
