import 'dart:typed_data';

import 'package:dotted_border/dotted_border.dart';
import 'package:ds_podi/ds_podi.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

export 'package:flutter_dropzone/flutter_dropzone.dart';

enum DropzoneType {
  image,
  csv,
  pdf,
  svg;
}

class DropzoneWidget extends StatefulWidget {
  final String title;
  final String? resolution;
  final double height;
  final double width;

  /// File:
  /// - file.name;
  /// - file.mime;
  /// - file.bytes;
  /// - file.url;
  final void Function(DroppedFile file) onDroppedItem;
  final void Function(int index) onRemove;
  @Deprecated(
      "Agora o texto de dentro do Dropzone virá direto do tipo de arquivo que este aceita!")
  final String text;
  final List<String> imageList;
  final bool hasHeader, hasButton, hasTitle, busy;
  final bool isSingleImage;
  final DropzoneType type;
  final String? image;
  const DropzoneWidget({
    Key? key,
    this.title = "Fotos",
    this.text = "Clique ou arraste um arquivo",
    this.resolution,
    this.height = 160.0,
    this.width = double.infinity,
    this.hasHeader = true,
    this.hasButton = true,
    this.hasTitle = true,
    this.isSingleImage = false,
    this.busy = false,
    this.type = DropzoneType.image,
    this.imageList = const [],
    this.image,
    required this.onDroppedItem,
    required this.onRemove,
  }) : super(key: key);

  @override
  _DropzoneWidgetState createState() => _DropzoneWidgetState();
}

class _DropzoneWidgetState extends State<DropzoneWidget> {
  late DropzoneViewController controller;
  final _scrollController = ScrollController();

  bool hovering = false;

  Widget _buildDropArea() {
    return DottedBorder(
      borderType: BorderType.RRect,
      dashPattern: [3, 6],
      color: PodiColors.neutrals[200]!,
      radius: Radius.circular(8),
      child: Container(
        width: widget.width,
        height: widget.height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: hovering ? PodiColors.neutrals[100] : PodiColors.neutrals[50],
        ),
        padding: const EdgeInsets.all(16),
      ),
    );
  }

  Widget _buildEmpty() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            color: PodiColors.white,
            border: Border.all(color: PodiColors.neutrals[300]!),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Center(child: Icon(PodiWebIcons.upload)),
        ),
        const SizedBox(height: 16),
        Text.rich(
          TextSpan(
            children: [
              TextSpan(
                text: "Clique para enviar",
                style: PodiTexts.label2.weightBold,
              ),
              TextSpan(
                text: " ou arraste aqui",
                style: PodiTexts.label2.weightMedium,
              ),
            ],
          ),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 4),
        Text(
          switch (widget.type) {
                DropzoneType.csv => "CSV",
                DropzoneType.pdf => "PDF",
                DropzoneType.svg => "SVG",
                DropzoneType.image => "PNG ou JPG",
              } +
              ((widget.resolution != null) ? " (${widget.resolution})" : ""),
          style: PodiTexts.label3.weightRegular
              .withColor(PodiColors.neutrals[700]!),
        ),
      ],
    );
  }

  Widget _buildImageList() {
    return SizedBox(
      width: widget.width,
      height: widget.height,
      child: RawScrollbar(
        thumbVisibility: true,
        controller: _scrollController,
        thumbColor: PodiColors.neutrals[900]!.withOpacity(0.5),
        radius: Radius.circular(8),
        thickness: 5,
        interactive: true,
        child: ListView.separated(
          padding: const EdgeInsets.all(12.0),
          controller: _scrollController,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border:
                        Border.all(color: PodiColors.neutrals[100]!, width: 2),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: Image.network(
                      widget.imageList[index],
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                Positioned(
                  top: 8,
                  right: 8,
                  child: InkWell(
                    onTap: () {
                      widget.onRemove(index);
                      setState(() {});
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: PodiColors.white),
                      padding: EdgeInsets.all(4),
                      child: SvgPicture.asset(
                        PodiIcons.trash,
                        height: 16,
                        width: 16,
                        colorFilter: ColorFilter.mode(
                          PodiColors.danger,
                          BlendMode.srcIn,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
          separatorBuilder: (context, index) => SizedBox(width: 16),
          itemCount: widget.imageList.length,
        ),
      ),
    );
  }

  Widget _buildSingleImage() {
    return Container(
      margin: const EdgeInsets.all(12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        border: Border.all(color: PodiColors.neutrals[100]!, width: 2),
      ),
      child: Stack(
        children: [
          SizedBox(
            width: widget.width - 24,
            height: widget.height - 24,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: Image.network(
                widget.image!,
                fit: BoxFit.contain,
              ),
            ),
          ),
          Positioned(
            top: 8,
            right: 8,
            child: InkWell(
              onTap: () {
                widget.onRemove(0);
                setState(() {});
              },
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: PodiColors.white),
                padding: EdgeInsets.all(4),
                child: SvgPicture.asset(
                  PodiIcons.trash,
                  height: 16,
                  width: 16,
                  colorFilter: ColorFilter.mode(
                    PodiColors.danger,
                    BlendMode.srcIn,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        if (widget.hasHeader) ...[
          SizedBox(
            width: widget.width,
            child: Row(
              mainAxisAlignment: (!widget.hasTitle || !widget.hasButton)
                  ? MainAxisAlignment.start
                  : MainAxisAlignment.spaceBetween,
              children: [
                if (widget.hasTitle)
                  Text(
                    widget.title,
                    style: PodiTexts.body1.weightMedium.heightRegular,
                  ),
                if (widget.hasButton)
                  PodiButton.secondary(
                    onTap: () async {
                      pickFiles();
                    },
                    size: ButtonSize.small,
                    icon: Icons.add,
                    title: "Adicionar",
                  ),
              ],
            ),
          ),
          SizedBox(height: 16),
        ],
        Stack(
          children: [
            SizedBox(
              height: widget.height,
              width: widget.width,
              child: _buildDropArea(),
            ),
            SizedBox(
              height: widget.height,
              width: widget.width,
              child: DropzoneView(
                onCreated: (controller) => this.controller = controller,
                onDrop: (file) {
                  if (widget.busy) return;
                  setState(() => hovering = false);
                  uploadFile(file);
                },
                onHover: () {
                  if (widget.busy) return;
                  setState(() => hovering = true);
                },
                onLeave: () {
                  if (widget.busy) return;
                  setState(() => hovering = false);
                },
              ),
            ),
            if (widget.isSingleImage) ...[
              if (isNull(widget.image)) ...[
                Positioned.fill(child: _buildEmpty()),
              ] else ...[
                _buildSingleImage(),
              ],
            ] else ...[
              if (widget.imageList.isEmpty) ...[
                Positioned.fill(child: _buildEmpty()),
              ] else ...[
                _buildImageList(),
              ],
            ],
            if (isNull(widget.image) && widget.imageList.isEmpty)
              InkWell(
                onTap: () async {
                  if (widget.busy) return;
                  await pickFiles();
                },
                borderRadius: BorderRadius.circular(8),
                child: SizedBox(
                  height: widget.height,
                  width: widget.width,
                ),
              ),
            if (widget.busy) ...[
              Container(
                height: widget.height,
                width: widget.width,
                color: PodiColors.neutrals[100]!.withOpacity(0.5),
                child: const Center(
                  child: SizedBox(
                    height: 48,
                    width: 48,
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(PodiColors.green),
                      strokeWidth: 3,
                    ),
                  ),
                ),
              ),
            ],
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }

  Future uploadFile(dynamic event) async {
    final supportedMimeFiles = switch (widget.type) {
      DropzoneType.csv => ["application/vnd.ms-excel"],
      DropzoneType.pdf => ["application/pdf"],
      DropzoneType.svg => ["image/svg+xml"],
      DropzoneType.image => ["image/jpeg", "image/png"],
    };

    final name = event.name;
    final mime = await controller.getFileMIME(event);
    final bytes = await controller.getFileSize(event);
    final url = await controller.createFileUrl(event);
    final data = await controller.getFileData(event);
    if (supportedMimeFiles.contains(mime)) {
      final droppedFile = DroppedFile(
        url: url,
        name: name,
        mime: mime,
        bytes: bytes,
        data: data,
      );
      widget.onDroppedItem(droppedFile);
      setState(() {});
    }
  }

  Future pickFiles() async {
    final supportedMimeFiles = switch (widget.type) {
      DropzoneType.csv => ["application/vnd.ms-excel"],
      DropzoneType.pdf => ["application/pdf"],
      DropzoneType.svg => ["image/svg+xml"],
      DropzoneType.image => ["image/jpeg", "image/png"],
    };
    final events =
        await controller.pickFiles(multiple: true, mime: supportedMimeFiles);
    if (events.isEmpty) return;
    uploadFile(events.first);
  }
}

class DroppedFile {
  final String url;
  final String name;
  final String mime;
  final int bytes;
  final Uint8List data;

  DroppedFile({
    required this.url,
    required this.name,
    required this.mime,
    required this.bytes,
    required this.data,
  });

  String get size {
    final kb = bytes / 1024;
    final mb = kb / 1024;

    return mb > 1
        ? "${mb.toStringAsFixed(2)} MB"
        : "${kb.toStringAsFixed(2)} KB";
  }
}
