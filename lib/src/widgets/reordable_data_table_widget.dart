import 'package:flutter/material.dart';

import '../../ds_podi.dart';

class ReordableDataTableWidget<T> extends StatefulWidget {
  final List<T> items;
  final List<ReordableDataTableColumn<T>> columns;
  final Function(int start, int current) onReorder;
  final double lineHeight;
  const ReordableDataTableWidget({
    required this.items,
    required this.columns,
    required this.onReorder,
    this.lineHeight = 48,
    Key? key,
  }) : super(key: key);

  @override
  State<ReordableDataTableWidget> createState() =>
      _ReordableDataTableWidgetState<T>();
}

class _ReordableDataTableWidgetState<T>
    extends State<ReordableDataTableWidget> {
  Widget _buildHeader() {
    return Container(
      margin: const EdgeInsets.only(top: 24),
      decoration: BoxDecoration(
        border:
            Border(bottom: BorderSide(color: PodiColors.dark[50]!, width: 1)),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: List<Widget>.generate(widget.columns.length + 1, (index) {
          if (index == 0) {
            return SizedBox.square(dimension: 40);
          }
          final column = widget.columns[index - 1];
          if (column.width != null) {
            return Container(
              width: column.width,
              padding: const EdgeInsets.all(16.0),
              child: Text(
                column.title,
                style: PodiTexts.caption
                    .size(11)
                    .heightRegular
                    .withColor(PodiColors.dark[500]!),
              ),
            );
          }
          return Expanded(
            flex: column.flex,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                column.title,
                style: PodiTexts.caption
                    .size(11)
                    .heightRegular
                    .withColor(PodiColors.dark[500]!),
              ),
            ),
          );
        }),
      ),
    );
  }

  List<Widget> _buildItems() {
    return List<Widget>.generate(widget.items.length, (index) {
      final item = widget.items[index];
      return Container(
        key: Key(index.toString()), // TODO: Verificar se é o melhor jeito.
        decoration: BoxDecoration(
          color: (index % 2 == 0)
              ? PodiColors.white
              : PodiColors.dark[50]!.withOpacity(0.25),
          border:
              Border(bottom: BorderSide(color: PodiColors.dark[50]!, width: 1)),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: List<Widget>.generate(widget.columns.length + 1, (i) {
            if (i == 0) {
              return ReorderableDragStartListener(
                index: index,
                child: SizedBox.square(
                  dimension: 40,
                  child: MouseRegion(
                    cursor: SystemMouseCursors.grab,
                    child: Center(
                      child: Icon(
                        PodiWebIcons.drag,
                        color: PodiColors.neutrals[900],
                        size: 16,
                      ),
                    ),
                  ),
                ),
              );
            }
            ReordableDataTableColumn<T> column =
                widget.columns[i - 1] as ReordableDataTableColumn<T>;
            if (column.width != null) {
              return Container(
                width: column.width,
                padding: const EdgeInsets.all(16.0),
                child: column.buildItem(item),
              );
            }
            return Expanded(
              flex: column.flex,
              // TODO: Vou ter que fazer assim para o DataTableWidget tbm
              child: LayoutBuilder(
                builder: (context, constraints) {
                  return Container(
                    height: widget.lineHeight,
                    padding: const EdgeInsets.only(left: 16),
                    child: Align(
                      child: column.buildItem(item),
                      alignment: Alignment.centerLeft,
                    ),
                  );
                },
              ),
            );
          }),
        ),
      );
    });
  }

  Widget _buildBody() {
    return ReorderableListView(
      buildDefaultDragHandles: false,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      header: _buildHeader(),
      children: _buildItems(),
      onReorder: widget.onReorder,
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }
}

class ReordableDataTableColumn<T> {
  final String title;
  final Function(T item)? onTap;
  final Widget Function(T item) buildItem;
  final int flex;
  final double? width;

  ReordableDataTableColumn({
    required this.title,
    required this.buildItem,
    this.flex = 1,
    this.width,
    this.onTap,
  });
}
