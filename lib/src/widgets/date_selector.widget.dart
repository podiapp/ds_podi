import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../ds_podi.dart';

class DateSelectorWidget extends StatefulWidget {
  final String dateSelected, rangeSelected;
  final Function(DateTime? startDate, DateTime? endDate)? onSelectionChanged;
  final Function(DateType value)? onDateTypeChanged;
  final Function() onSaved, onCancel;
  final Function()? onOpenDialog;
  final Widget child;
  final DateTime startDate, endDate;
  final DateTime? initialSelectedDate, endSelectedDate;
  final DateRangePickerView? calendarView;
  final bool isSingleSelection;
  const DateSelectorWidget({
    required this.child,
    required this.rangeSelected,
    required this.dateSelected,
    required this.onSaved,
    required this.onCancel,
    required this.startDate,
    required this.endDate,
    this.isSingleSelection = false,
    this.calendarView,
    this.onDateTypeChanged,
    this.initialSelectedDate,
    this.endSelectedDate,
    this.onOpenDialog,
    this.onSelectionChanged,
    Key? key,
  }) : super(key: key);

  @override
  State<DateSelectorWidget> createState() => _DateSelectorWidgetState();
}

class _DateSelectorWidgetState extends State<DateSelectorWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (widget.onOpenDialog != null) {
          widget.onOpenDialog!();
        }
        bool? r = await showDialog(
          context: context,
          builder: (_) => DateSelectorDialog(
            onSelectionChanged: widget.onSelectionChanged,
            child: widget.child,
            startDate: widget.startDate,
            onDateTypeChanged: widget.onDateTypeChanged,
            endDate: widget.endDate,
            initialSelectedDate: widget.initialSelectedDate,
            endSelectedDate: widget.endSelectedDate,
            calendarView: widget.calendarView,
          ),
        );
        if (r == true) {
          widget.onSaved();
          setState(() {});
        } else {
          widget.onCancel();
        }
      },
      child: Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              decoration: BoxDecoration(
                color: PodiColors.dark[100],
                borderRadius: BorderRadius.circular(4),
              ),
              child: Center(
                child: Text(
                  widget.rangeSelected,
                  style: PodiTexts.caption.size(11).heightCenter,
                ),
              ),
            ),
            const SizedBox(width: 8),
            Text(
              widget.dateSelected,
              style: PodiTexts.body1.weightMedium.heightRegular,
            ),
            const SizedBox(width: 16),
            const Icon(Icons.arrow_drop_down),
          ],
        ),
      ),
    );
  }
}

class DateSelectorDialog extends StatefulWidget {
  final Function(DateTime? startDate, DateTime? endDate)? onSelectionChanged;
  final Function(DateType value)? onDateTypeChanged;
  final Widget child;
  final DateTime startDate, endDate;
  final DateTime? initialSelectedDate, endSelectedDate;
  final DateRangePickerView? calendarView;
  final bool isSingleSelection;
  const DateSelectorDialog({
    required this.child,
    required this.startDate,
    required this.endDate,
    this.isSingleSelection = false,
    this.calendarView,
    this.onDateTypeChanged,
    this.initialSelectedDate,
    this.endSelectedDate,
    this.onSelectionChanged,
    Key? key,
  }) : super(key: key);

  @override
  State<DateSelectorDialog> createState() => _DateSelectorDialogState();
}

class _DateSelectorDialogState extends State<DateSelectorDialog> {
  final _controller = DateRangePickerController();
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(16)),
                child: Container(
                  height: 424,
                  width: 180,
                  decoration: BoxDecoration(
                    color: PodiColors.white,
                    border: Border(
                      right: BorderSide(
                        color: PodiColors.dark.withOpacity(0.1),
                      ),
                    ),
                  ),
                  padding: const EdgeInsets.all(24),
                  child: widget.child,
                ),
              ),
              Container(
                width: 434,
                height: 423,
                padding: const EdgeInsets.all(24),
                decoration: const BoxDecoration(
                  color: PodiColors.white,
                  borderRadius: BorderRadius.only(topRight: Radius.circular(16)),
                ),
                child: SfDateRangePicker(
                  view: widget.calendarView ?? DateRangePickerView.month,
                  allowViewNavigation: !(widget.calendarView != null),
                  initialDisplayDate: DateTime.now(),
                  controller: _controller,
                  minDate: widget.startDate,
                  maxDate: widget.isSingleSelection ? null : widget.endDate,
                  onSelectionChanged: (DateRangePickerSelectionChangedArgs v) {
                    var value = v.value;
                    if (widget.onSelectionChanged != null) {
                      if (widget.isSingleSelection) {
                        widget.onSelectionChanged!(value, DateTime.now());
                      } else {
                        widget.onSelectionChanged!(value.startDate, value.endDate);
                      }
                    }
                  },
                  initialSelectedRange: widget.isSingleSelection
                      ? null
                      : PickerDateRange(widget.initialSelectedDate, widget.endSelectedDate),
                  enablePastDates: true,
                  enableMultiView: false,
                  navigationMode: DateRangePickerNavigationMode.scroll,
                  navigationDirection: DateRangePickerNavigationDirection.vertical,
                  selectionMode: widget.isSingleSelection
                      ? DateRangePickerSelectionMode.single
                      : DateRangePickerSelectionMode.range,
                  monthViewSettings: DateRangePickerMonthViewSettings(
                    firstDayOfWeek: 1,
                    viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: PodiTexts.button2.size(13),
                    ),
                  ),
                  headerStyle: DateRangePickerHeaderStyle(
                    textStyle: PodiTexts.button1.size(13).withColor(PodiColors.purple),
                  ),
                  cellBuilder: (context, cellDetails) {
                    switch (_controller.view) {
                      case DateRangePickerView.month:
                        bool isDateValid = (cellDetails.date.isAtSameMomentAs(widget.startDate) ||
                                cellDetails.date.isAtSameMomentAs(widget.endDate)) ||
                            (cellDetails.date.isAfter(widget.startDate) &&
                                cellDetails.date.isBefore(widget.endDate));

                        bool isToday = DateTime(
                                cellDetails.date.year, cellDetails.date.month, cellDetails.date.day)
                            .isAtSameMomentAs(DateTime(
                                DateTime.now().year, DateTime.now().month, DateTime.now().day));

                        return Container(
                          width: cellDetails.bounds.width,
                          height: cellDetails.bounds.height,
                          alignment: Alignment.center,
                          color: (isToday) ? PodiColors.purple : null,
                          child: Text(
                            cellDetails.date.day.toString(),
                            style: PodiTexts.button1.size(13).withColor(
                                  (isToday)
                                      ? PodiColors.white
                                      : (isDateValid)
                                          ? PodiColors.dark
                                          : PodiColors.dark[500]!,
                                ),
                          ),
                        );
                      case DateRangePickerView.year:
                        String month =
                            DateFormat("MMMM", "pt-BR").format(cellDetails.date).toUpperCase();

                        DateTime cellDate =
                            DateTime(cellDetails.date.year, cellDetails.date.month, 01);

                        DateTime startMonth =
                            DateTime(widget.startDate.year, widget.startDate.month, 01);

                        DateTime endMonth = DateTime(widget.endDate.year, widget.endDate.month, 01);

                        bool isDateValid = (cellDate.isAtSameMomentAs(startMonth) ||
                                cellDate.isAtSameMomentAs(endMonth)) ||
                            (cellDate.isAfter(startMonth) && cellDetails.date.isBefore(endMonth));

                        return Container(
                          width: cellDetails.bounds.width,
                          height: cellDetails.bounds.height,
                          alignment: Alignment.center,
                          child: Text(
                            month,
                            style: PodiTexts.button1
                                .size(13)
                                .withColor((isDateValid) ? PodiColors.dark : PodiColors.dark[500]!),
                          ),
                        );
                      case DateRangePickerView.decade:
                        DateTime cellDate = DateTime(cellDetails.date.year, 01, 01);

                        DateTime startYear = DateTime(widget.startDate.year, 01, 01);

                        DateTime endYear = DateTime(widget.endDate.year, 01, 01);

                        bool isDateValid = (cellDate.isAtSameMomentAs(startYear) ||
                                cellDate.isAtSameMomentAs(endYear)) ||
                            (cellDate.isAfter(startYear) && cellDetails.date.isBefore(endYear));
                        return Container(
                          width: cellDetails.bounds.width,
                          height: cellDetails.bounds.height,
                          alignment: Alignment.center,
                          child: Text(
                            cellDetails.date.year.toString(),
                            style: PodiTexts.button1
                                .size(13)
                                .withColor((isDateValid) ? PodiColors.dark : PodiColors.dark[500]!),
                          ),
                        );
                      default:
                        final int yearValue = (cellDetails.date.year ~/ 10) * 10;
                        return Container(
                          width: cellDetails.bounds.width,
                          height: cellDetails.bounds.height,
                          alignment: Alignment.center,
                          child: Text(
                            yearValue.toString() + ' - ' + (yearValue + 9).toString(),
                            style: PodiTexts.button1.size(13).withColor(PodiColors.dark),
                          ),
                        );
                    }
                  },
                ),
              ),
            ],
          ),
          ClipRRect(
            borderRadius: const BorderRadius.vertical(bottom: Radius.circular(16)),
            child: Container(
              width: 614,
              height: 64,
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
              decoration: BoxDecoration(
                color: PodiColors.white,
                border: Border(
                  top: BorderSide(
                    color: PodiColors.dark.withOpacity(0.1),
                  ),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () => Navigator.pop(context, false),
                    child: Text(
                      "Cancelar",
                      style: PodiTexts.button2.heightCenter.weightBold
                          .withColor(PodiColors.dark[300]!),
                    ),
                  ),
                  const SizedBox(width: 24),
                  GreenButton(
                    title: "Aplicar",
                    onTap: () => Navigator.pop(context, true),
                    style: PodiTexts.button2.heightCenter.weightBold.withColor(PodiColors.white),
                    radius: 6,
                    width: 80,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
