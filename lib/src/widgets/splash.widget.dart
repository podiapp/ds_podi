import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../utils.dart';

class StartupPage extends StatelessWidget {
  final Future<void> Function() startup;
  const StartupPage(this.startup, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => startup());
    return Scaffold(
      backgroundColor: PodiColors.green[900],
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: RadialGradient(
              colors: [PodiColors.green, PodiColors.green[900]!]),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image.asset(DSIcons.splashBackground,
                height: double.infinity,
                width: double.infinity,
                repeat: ImageRepeat.repeat),
            SvgPicture.asset(PodiIcons.logoPodiWhite),
          ],
        ),
      ),
    );
  }
}
