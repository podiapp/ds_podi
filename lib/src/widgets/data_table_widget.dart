import 'package:ds_podi/ds_podi.dart';
import 'package:flutter/material.dart';

class DataTableWidget<T> extends StatefulWidget {
  final List<DataTableColumn<T>> columns;
  final List<T> items;
  final double lineHeight;

  const DataTableWidget({
    required this.columns,
    required this.items,
    this.lineHeight = 48,
    Key? key,
  }) : super(key: key);

  @override
  State<DataTableWidget> createState() => _DataTableWidgetState<T>();
}

class _DataTableWidgetState<T> extends State<DataTableWidget> {
  List<DataRow> get _rows {
    List<DataRow> result = [];
    for (int i = 0; i < widget.items.length; i++) {
      final item = widget.items[i];
      List<DataCell> cells = [];
      widget.columns.forEach(
        (c) => cells.add(
          c.buildCell(item),
        ),
      );
      result.add(DataRow(
        selected: false,
        color: WidgetStateProperty.all((i % 2 == 0)
            ? PodiColors.neutrals[50]!.withOpacity(0.25)
            : PodiColors.white),
        cells: cells,
      ));
    }
    return result;
  }

  List<DataColumn> get _columns {
    List<DataColumn> result = [];
    for (var column in widget.columns) {
      result.add(
        DataColumn(
          onSort: column.onTapHeader != null
              ? (_, __) {
                  column.onTapHeader?.call();
                }
              : null,
          label: Text(
            column.title,
            style: PodiTexts.overline.heightCenter.weightBold
                .withColor(PodiColors.neutrals[500]!),
          ),
        ),
      );
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: _columns,
      rows: _rows,
      decoration: BoxDecoration(color: PodiColors.white),
      dataRowMinHeight: widget.lineHeight,
      columnSpacing: 24,
      headingRowHeight: 48,
      headingRowColor: WidgetStateProperty.all(PodiColors.white),
      horizontalMargin: 16,
    );
  }
}

class DataTableColumn<T> {
  final String title;
  final Function()? onTapHeader;
  final Function(T item)? onTapItem;
  final Widget Function(T item) buildItem;

  DataTableColumn({
    required this.title,
    required this.buildItem,
    this.onTapHeader,
    this.onTapItem,
  });

  DataCell buildCell(T item) {
    return DataCell(
      buildItem(item),
      onTap: onTapItem != null ? () => onTapItem?.call(item) : null,
    );
  }
}
