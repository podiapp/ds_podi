import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

import '../utils.dart';

///Widget que simula um texto com largura definida [width] para ficar como placeholder durante o carregamento da página
class FakeText extends StatelessWidget {
  final int lines;
  final TextStyle? style;
  final double? width;
  const FakeText({Key? key, this.lines = 1, this.style, this.width}) : super(key: key);

  Widget line() => Container(
        height: style!.height! * style!.fontSize!,
        width: width ?? double.infinity,
        color: PodiColors.dark[500],
      );

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(lines,
            (_) => Padding(padding: const EdgeInsets.symmetric(vertical: 2), child: line())));
  }
}

///Simula um item de lista fake como placeholder para carregamentos
///
///Utilizado quando não se tem controle do resultado final, ou seja, como um placeholder genérico
class BaseFake extends StatelessWidget {
  const BaseFake({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(height: 80, width: 80, color: PodiColors.dark[500]),
        SizedBox(width: 16),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FakeText(style: PodiTexts.subtitle1),
              SizedBox(height: 4),
              FakeText(style: PodiTexts.caption, width: Get.size.width - 176),
              SizedBox(height: 4),
              FakeText(style: PodiTexts.caption, width: Get.size.width - 176),
            ],
          ),
        )
      ],
    );
  }
}

///Controla o efeito de shimmer através da variável [enabled]
///
///Não deve ser utilizado na web até estabilização do [Shimmer] na Web
class ShimmerWidget extends StatelessWidget {
  final Widget? child;
  final bool enabled;
  final Duration period;
  const ShimmerWidget(
      {this.child, this.period = const Duration(milliseconds: 1500), this.enabled = true, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) => enabled
      ? Shimmer.fromColors(
          period: period,
          child: child!,
          baseColor: Color(0xFFF9F9FB),
          highlightColor: Color(0XFFE0E0EB),
        )
      : child!;
}
