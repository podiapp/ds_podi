import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../utils.dart';
import 'network_image.widget.dart';

class ImageCarouselWidget extends StatefulWidget {
  final List<String> photos;
  final double? height, width;
  final Function(int)? onTap;
  final Duration swipeDuration;
  final bool fromAssets, adaptativeMarkers;
  const ImageCarouselWidget({
    required this.photos,
    this.height,
    this.width,
    this.onTap,
    this.swipeDuration = const Duration(seconds: 3),
    this.fromAssets = false,
    this.adaptativeMarkers = false,
  });
  @override
  State<StatefulWidget> createState() => _ImageCarouselWidget();
}

class _ImageCarouselWidget extends State<ImageCarouselWidget> {
  int _current = 0;
  late List<Widget> slides;

  @override
  void initState() {
    super.initState();
    List<String> photos = widget.photos;
    if (photos.length > 8) photos = photos.sublist(0, 8);
    if (photos.isEmpty) {
      slides = [];
    } else {
      slides = photos
          .where((i) => !isNull(i))
          .map((i) => widget.fromAssets
              ? Image.asset(
                  i,
                  height: widget.height,
                  width: widget.width,
                  fit: BoxFit.cover,
                )
              : NetworkImageWidget(
                  i,
                  height: widget.height,
                  width: widget.width,
                  fit: BoxFit.cover,
                  openImage: widget.onTap == null,
                ))
          .toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.photos.isEmpty)
      return Center(
        child: Icon(
          PodiIcons.podi,
          color: PodiColors.dark[100],
          size: 64,
        ),
      );

    return LayoutBuilder(
      builder: (_, c) {
        final widgetWidth =
            (widget.width == double.infinity || widget.width == null)
                ? c.constrainWidth()
                : widget.width;

        return Stack(
          children: [
            GestureDetector(
              onTap: () {
                widget.onTap?.call(_current);
              },
              child: CarouselSlider(
                items: slides,
                options: CarouselOptions(
                  autoPlay: slides.length > 1,
                  scrollPhysics: slides.length > 1
                      ? AlwaysScrollableScrollPhysics()
                      : NeverScrollableScrollPhysics(),
                  viewportFraction: 1.0,
                  autoPlayInterval: widget.swipeDuration,
                  enlargeCenterPage: false,
                  aspectRatio: 1.0,
                  height: widget.height,
                  onPageChanged: (index, _) => setState(() => _current = index),
                ),
              ),
            ),
            if (slides.length > 1) ...[
              if (widget.adaptativeMarkers) ...[
                Positioned(
                  bottom: 12,
                  left: 12,
                  right: 12,
                  height: 6,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(slides.length, (index) {
                      var width = _current == index ? 16.0 : 6.0;
                      return AnimatedContainer(
                        duration: Duration(milliseconds: 200),
                        width: width,
                        height: 6,
                        margin: EdgeInsets.symmetric(horizontal: 4),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3),
                          color: PodiColors.white,
                        ),
                      );
                    }),
                  ),
                ),
              ] else ...[
                Positioned(
                  bottom: 16,
                  left: 16,
                  right: 16,
                  height: 4,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: List.generate(slides.length, (index) {
                      var color = _current != index
                          ? const Color(0x80FFFFFF)
                          : PodiColors.white;
                      var width = (widgetWidth! - 32 - 4 * (slides.length)) /
                          slides.length;
                      if (width < 0) width = 4;
                      return Container(
                        constraints: BoxConstraints(maxWidth: 61.6),
                        width: width,
                        height: 4,
                        margin: EdgeInsets.only(right: 2.0, left: 2),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2),
                            color: color),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ],
          ],
        );
      },
    );
  }
}
