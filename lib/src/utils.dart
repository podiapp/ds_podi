import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart' hide TextDirection;
import 'package:logger/logger.dart';
import 'package:validators/validators.dart';

export 'package:dio/dio.dart';
export 'package:syncfusion_flutter_charts/charts.dart' hide Position;
export 'package:validators/validators.dart';

export 'formatters/money_input_formatter.dart';
export 'formatters/upper_case_text_formatter.dart';
export 'utils.dart';

part 'utils/api_constants.dart';
part 'utils/colors.dart';
part 'utils/extensions.dart';
part 'utils/functions.dart';
part 'utils/icons.dart';
part 'utils/logger.dart';
part 'utils/routes.dart';
part 'utils/styles.dart';
part 'utils/textStyles.dart';
