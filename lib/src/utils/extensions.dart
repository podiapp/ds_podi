part of '../utils.dart';

extension StringExtension on String {
  String hide([int? start, int? end]) {
    List<String> chars = split("");
    List<String> replacement =
        List.generate(length - ((start ?? 1) + (end ?? 1)), (_) => "*");
    chars.replaceRange(
      start ?? 1,
      length - (end ?? 1),
      replacement,
    );
    return chars.join();
  }
}

extension CustomDateTime on DateTime {
  String formatted([String pattern = "dd/MM/yyyy - HH:mm"]) {
    initializeDateFormatting('pt_BR');
    return DateFormat(pattern, 'pt_BR').format(this);
  }

  String toStringWithLocalOffset() {
    String time = toIso8601String();
    int offset = DateTime.now().timeZoneOffset.inHours;
    if (offset > 0) {
      time += "+${offset.toString().padLeft(2, '0')}:00";
    } else if (offset < 0) {
      time += "-${(-offset).toString().padLeft(2, '0')}:00";
    }
    return time;
  }

  String getDuration() {
    String text = "";
    // date = date.subtract(const Duration(days: ));
    if (year > 1) {
      if (!isNull(text)) text += " ";
      text += "${year - 1} ${year > 2 ? "anos" : "ano"}";
    }
    if (month > 1) {
      if (!isNull(text)) text += " ";
      text += "${month - 1} ${month > 2 ? "meses" : "mês"}";
    }
    if (day > 1) {
      if (!isNull(text)) text += " ";
      text += "${day - 1} ${day > 2 ? "dias" : "dia"}";
    }
    if (hour > 0) {
      if (!isNull(text)) text += " ";
      text += "$hour ${hour > 1 ? "horas" : "hora"}";
    }
    if (minute > 0) {
      if (!isNull(text)) text += " ";
      text += "$minute ${minute > 1 ? "minutos" : "minuto"}";
    }
    return text;
  }
}
