part of "../utils.dart";

/// Classe onde se armazena todas as constantes da API utilizada nos serviços PODI.
class ApiConstants {
  /// Nessa variável, se armazena qual `environment` está sendo utilizada.
  static const _env = String.fromEnvironment("ENV");

  static ENV get env {
    switch (_env) {
      case "dev":
        return ENV.DEVELOPMENT;
      case "sb":
        return ENV.SANDBOX;
      default:
        return ENV.PRODUCTION;
    }
  }

  static const suffix = _env != "" ? "-$_env" : "";
  //APIs
  /// API de Autorização
  ///
  /// https://auth-sb.podiapp.com.br/swagger/index.html \
  /// https://auth-dev.podiapp.com.br/swagger/index.html \
  /// https://auth.podiapp.com.br/swagger/index.html
  static const String API_AUTH = "https://auth$suffix.podiapp.com.br/api/";

  /// API de Importação
  ///
  /// https://importer-sb.podiapp.com.br/swagger/index.html \
  /// https://importer-dev.podiapp.com.br/swagger/index.html \
  /// https://importer.podiapp.com.br/swagger/index.html
  static const String API_IMPORT =
      "https://importer$suffix.podiapp.com.br/api/";

  /// API de Pushs
  ///
  /// https://push-sb.podiapp.com.br/swagger/index.html \
  /// https://push-dev.podiapp.com.br/swagger/index.html \
  /// https://push.podiapp.com.br/swagger/index.html
  static const String API_PUSH = "https://push$suffix.podiapp.com.br/api/";

  /// API de Shoppings
  ///
  /// https://malls-sb.podiapp.com.br/swagger/index.html \
  /// https://malls-dev.podiapp.com.br/swagger/index.html \
  /// https://malls.podiapp.com.br/swagger/index.html
  static const String API_MALLS = "https://malls$suffix.podiapp.com.br/api/";

  /// API de Lojas
  ///
  /// https://stores-sb.podiapp.com.br/swagger/index.html \
  /// https://stores-dev.podiapp.com.br/swagger/index.html \
  /// https://stores.podiapp.com.br/swagger/index.html
  static const String API_STORES = "https://stores$suffix.podiapp.com.br/api/";

  /// API de Eventos
  ///
  /// https://events-sb.podiapp.com.br/swagger/index.html \
  /// https://events-dev.podiapp.com.br/swagger/index.html \
  /// https://events.podiapp.com.br/swagger/index.html
  static const String API_EVENTS = "https://events$suffix.podiapp.com.br/api/";

  /// API de Cinemas
  ///
  /// https://cinemas-sb.podiapp.com.br/swagger/index.html \
  /// https://cinemas-dev.podiapp.com.br/swagger/index.html \
  /// https://cinemas.podiapp.com.br/swagger/index.html
  static const String API_MOVIES = "https://cinemas$suffix.podiapp.com.br/api/";

  /// API de Checkouts
  ///
  /// https://checkout-sb.podiapp.com.br/swagger/index.html \
  /// https://checkout-dev.podiapp.com.br/swagger/index.html \
  /// https://checkout.podiapp.com.br/swagger/index.html
  static const String API_CHECKOUT =
      "https://checkout$suffix.podiapp.com.br/api/";

  /// API de Campanhas
  ///
  /// https://campaign-sb.podiapp.com.br/swagger/index.html \
  /// https://campaign-dev.podiapp.com.br/swagger/index.html \
  /// https://campaign.podiapp.com.br/swagger/index.html
  static const String API_CAMPAIGN =
      "https://campaign$suffix.podiapp.com.br/api/";

  /// API de Estacionamento
  ///
  /// https://parking-sb.podiapp.com.br/swagger/index.html \
  /// https://parking-dev.podiapp.com.br/swagger/index.html \
  /// https://parking.podiapp.com.br/swagger/index.html
  static const String API_PARKING =
      "https://parking-lot$suffix.podiapp.com.br/api/";

  /// API de Configurações do Aplicativo
  ///
  /// https://appsettings-sb.podiapp.com.br/swagger/index.html \
  /// https://appsettings-dev.podiapp.com.br/swagger/index.html \
  /// https://appsettings.podiapp.com.br/swagger/index.html
  static const String API_SETTINGS =
      "https://appsettings$suffix.podiapp.com.br/api/";

  /// API de descontos e vitrine
  ///
  /// https://coupons-sb.podiapp.com.br/swagger/index.html \
  /// https://coupons-dev.podiapp.com.br/swagger/index.html \
  /// https://coupons.podiapp.com.br/swagger/index.html
  static const String API_COUPONS =
      "https://coupons$suffix.podiapp.com.br/api/";

  /// API de relatórios dos shoppings
  ///
  /// https://reports-sb.podiapp.com.br/swagger/index.html \
  /// https://reports-dev.podiapp.com.br/swagger/index.html \
  /// https://reports.podiapp.com.br/swagger/index.html
  static const String API_REPORTS = "https://reports$suffix.podiapp.com.br/";

  /// API de notificações
  ///
  /// https://notification-sb.podiapp.com.br/swagger/index.html \
  /// https://notification-dev.podiapp.com.br/swagger/index.html \
  /// https://notification.podiapp.com.br/swagger/index.html
  static const String API_NOTIFICATION =
      "https://notification$suffix.podiapp.com.br/api/";

  /// API de Clube Podi
  ///
  /// https://club-sb.podiapp.com.br/swagger/index.html \
  /// https://club-dev.podiapp.com.br/swagger/index.html \
  /// https://club.podiapp.com.br/swagger/index.html
  static const String API_CLUB = "https://club$suffix.podiapp.com.br/api/";

  /// API de notas fiscais
  ///
  /// https://invoices-sb.podiapp.com.br/swagger/index.html \
  /// https://invoices-dev.podiapp.com.br/swagger/index.html \
  /// https://invoices.podiapp.com.br/swagger/index.html
  static const String API_INVOICES =
      "https://invoices$suffix.podiapp.com.br/api/";

  /// API de relatorios gerais
  ///
  /// https://general-reports-sb.podiapp.com.br/swagger/index.html \
  /// https://general-reports-dev.podiapp.com.br/swagger/index.html \
  /// https://general-reports.podiapp.com.br/swagger/index.html
  static const String API_GENERAL_REPORTS =
      "https://general-reports$suffix.podiapp.com.br/api/";

  /// API de filas
  ///
  /// https://queue-sb.podiapp.com.br/swagger/index.html \
  /// https://queue-dev.podiapp.com.br/swagger/index.html \
  /// https://queue.podiapp.com.br/swagger/index.html
  static const String API_QUEUE = "https://queue$suffix.podiapp.com.br/api/";

  /// API de enquetes
  ///
  /// https://survey-sb.podiapp.com.br/swagger/index.html \
  /// https://survey-dev.podiapp.com.br/swagger/index.html \
  /// https://survey.podiapp.com.br/swagger/index.html
  static const String API_SURVEY = "https://survey$suffix.podiapp.com.br/api/";

  /// API de Big Data
  ///
  /// https://bigdata-sb.podiapp.com.br/swagger/index.html \
  /// https://bigdata-dev.podiapp.com.br/swagger/index.html \
  /// https://bigdata.podiapp.com.br/swagger/index.html
  static const String API_BIGDATA = "https://bigdata$suffix.podiapp.com.br/";

  /// API de Email Marketing
  ///
  /// https://email-marketing-sb.podiapp.com.br/swagger/index.html \
  /// https://email-marketing-dev.podiapp.com.br/swagger/index.html \
  /// https://email-marketing.podiapp.com.br/swagger/index.html
  static const String API_EMAILMKT =
      "https://email-marketing$suffix.podiapp.com.br/";

  /// API de RealTimeManagement
  ///
  /// https://rtm-sb.podiapp.com.br/swagger/index.html \
  /// https://rtm-dev.podiapp.com.br/swagger/index.html \
  /// https://rtm.podiapp.com.br/swagger/index.html
  static const String API_REALTIME = "https://rtm$suffix.podiapp.com.br/";
}

enum ENV { DEVELOPMENT, SANDBOX, PRODUCTION }

extension ENV_STRING on ENV {
  String get name {
    switch (this) {
      case ENV.DEVELOPMENT:
        return "development";
      case ENV.PRODUCTION:
        return "production";
      case ENV.SANDBOX:
        return "sandbox";
    }
  }
}
