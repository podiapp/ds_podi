import '../utils.dart';

/// Classe modelo para os endereços.
///
/// * `String` zipcode;
/// * `String` streetName;
/// * `String` number;
/// * `String` complement;
/// * `String` neighborhood;
/// * `String` city;
/// * `String` name;
/// * `String` id;
class AddressModel {
  String? zipcode,
      streetName,
      number,
      complement,
      neighborhood,
      city,
      state,
      name,
      id;

  ///Construtor principal da classe `AddressModel`.
  AddressModel()
      : zipcode = "",
        streetName = "",
        number = "",
        complement = "",
        neighborhood = "",
        city = "",
        state = "",
        name = "",
        id = DateTime.now().millisecondsSinceEpoch.toString();

  AddressModel.fromJson(Map<String, dynamic> json)
      : zipcode = json["zipcode"],
        streetName = json["streetName"],
        number = json["number"],
        complement = json["complement"],
        neighborhood = json["neighborhood"],
        city = json["city"],
        state = json["state"],
        id = json["addressId"],
        name = json["title"] ?? "Sem Nome";

  Map<String, dynamic> toJson() => {
        'title': name?.trim(),
        'zipcode': zipcode?.replaceAll(new RegExp('([^0-9])'), ""),
        'streetName': streetName?.trim(),
        'number': number?.trim(),
        'complement': complement?.trim(),
        'neighborhood': neighborhood?.trim(),
        'city': city.toString().trim(),
        'state': state.toString().trim(),
        'addressId': id != "" ? id : null,
      };

  @override
  String toString() =>
      "$streetName, $number, ${(isNull(complement?.trim())) ? "" : "$complement, "}$neighborhood - $city, $state, $zipcode";

  String toStringHidden() =>
      "${streetName?.hide(4)}, ${number?.hide(1, 0)}, ${(isNull(complement?.trim())) ? "" : "${complement?.hide(0, 2)}, "}$neighborhood, $city, $state";
}
