import 'package:intl/intl.dart';

class MovieModel {
  final String? id, mallId, partnership, theaterId, cityId, theaterName;
  String? title,
      mallName,
      trailerUrl,
      duration,
      rating,
      synopsis,
      portrait,
      horizontal,
      ingressoId,
      link;
  late List<String> genres;
  bool isSoon;

  // MovieModel.fromIngressoCom(
  //     Map<String, dynamic> json, MovieTheaterModel theater,
  //     {this.isSoon = false, this.link})
  //     : id = json['id'],
  //       partnership = theater.partnership,
  //       theaterId = theater.id,
  //       theaterName = theater.name,
  //       cityId = theater.cityId {
  //   title = json['title'];
  //   ingressoId = json['id'];
  //   trailerUrl = json['trailers'] is List && json['trailers'].isNotEmpty
  //       ? ((json['trailers'] as List).first ?? {})['url']
  //       : null;
  //   if (isSoon) link = null;
  //   duration = json['duration'];
  //   rating = json['contentRating'];
  //   synopsis = json['synopsis'];
  //   portrait = (json['images'] ?? [])?.firstWhere(
  //       (v) => v is Map && v['type'] == "PosterPortrait",
  //       orElse: () => {})['url'];
  //   horizontal = (json['images'] ?? [])?.firstWhere(
  //       (v) => v is Map && v['type'] == "PosterHorizontal",
  //       orElse: () => {})['url'];
  //   genres = (json['genres'] as List).map((v) => v.toString()).toList();
  // }

  MovieModel.fromJson(Map<String, dynamic> json)
      : id = json['movieId'],
        mallId = json['mallId'],
        mallName = json['mallName'],
        ingressoId = json['ingressoId'],
        partnership = json['partnership'],
        theaterId = json['theaterId'],
        cityId = json['cityId'],
        theaterName = json['theaterName'],
        title = json['title'],
        trailerUrl = json['trailerUrl'],
        duration = json['duration'],
        rating = json['rating'].toString(),
        synopsis = json['synopsis'],
        portrait = (json['images'] ?? [])?.firstWhere(
            (v) => v is Map && v['type'] == "PosterPortrait",
            orElse: () => {})['url'],
        horizontal = (json['images'] ?? [])?.firstWhere(
            (v) => v is Map && v['type'] == "PosterHorizontal",
            orElse: () => {})['url'],
        link = json['link'],
        genres = ((json['genres'] as List?) ?? []).map<String>((v) {
          return v.toString();
        }).toList(),
        isSoon = !json['isPlaying'];
}

class MovieTheaterModel {
  final String? id, name, url, corporation, partnership, cityId, blockMessage;
  late List<MovieModel> movies;

  List<MovieModel> get currentMovies => movies.where((m) => !m.isSoon).toList();
  List<MovieModel> get soonMovies => movies.where((m) => m.isSoon).toList();

  MovieTheaterModel.fromIngressoCom(Map<String, dynamic> json, this.partnership)
      : id = json['id'],
        name = json['name'],
        cityId = json['cityId'],
        url = json['siteUrl'],
        blockMessage = json['blockMessage'],
        corporation =
            json['corporation'].toString().toUpperCase().contains("IMAX")
                ? "IMAX"
                : json['corporation'];

  MovieTheaterModel.fromJson(Map<String, dynamic> json)
      : id = json['cinemaId'],
        name = json['name'],
        url = json['url'],
        corporation = json['corporation'],
        partnership = json['partnership'],
        cityId = json['cityId'],
        movies = ((json['movies'] as List?) ?? [])
            .map((m) => MovieModel.fromJson(m))
            .toList(),
        blockMessage = json['blockMessage'];
}

class MovieSession {
  String? id, url, time, room;
  DateTime? date;
  late List<String> properties;

  MovieSession.fromJson(Map<String, dynamic> json, this.room) {
    id = json['id'];
    url = json['siteURL'];
    time = json['time'];
    date = DateFormat("dd/MM/yyyy")
        .parse("${json['date']["dayAndMonth"]}/${json['date']["year"]}");
    properties = (json['types'] as List)
        .map<String>((t) => t['alias'].toString())
        .toList();
  }

  bool get isLeg => properties.contains("LEG");
  bool get isDub => properties.contains("DUB");

  bool hasType(String? type) => properties.contains(type);

  String get property => properties
      .firstWhere((p) => !["LEG", "DUB"].contains(p), orElse: () => "NORMAL");

  @override
  String toString() => "$id || $room - $date || $properties";
}
