class AppVersion implements Comparable {
  final int version;
  final AppVersion? subversion;

  AppVersion(String value)
      : version = int.tryParse(value.split('.')[0]) ?? 0,
        subversion = value.split('.').length > 1
            ? AppVersion((value.split('.')..removeAt(0)).join("."))
            : null;

  static AppVersion zero = AppVersion("0");

  @override
  int compareTo(other) {
    if (other is AppVersion) {
      final compare = version.compareTo(other.version);
      final hasSub = subversion != null || other.subversion != null;
      if (compare != 0 || !hasSub) return compare;
      return (subversion ?? AppVersion.zero)
          .compareTo(other.subversion ?? AppVersion.zero);
    } else if (other is String) {
      return toString().compareTo(other);
    } else if (other is int) {
      return version.compareTo(other);
    } else {
      throw ArgumentError("Can't compare ${other.runtimeType} to AppVersion");
    }
  }

  @override
  String toString() {
    if (subversion == null) return version.toString();
    return "$version.$subversion";
  }

  @override
  bool operator ==(other) => compareTo(other) == 0;

  @override
  int get hashCode => toString().hashCode;

  bool operator <(other) => compareTo(other) < 0;

  bool operator <=(other) => compareTo(other) <= 0;

  bool operator >(other) => compareTo(other) > 0;

  bool operator >=(other) => compareTo(other) >= 0;
}
