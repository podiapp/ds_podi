import '../utils.dart';

class MallStoreModel {
  final String storeId, mallId, title;
  final String? zipzId, logo, complement, place, phone, email, whatsappNumber, link;
  final bool useWhatsapp, hasNuvideo;
  final List<StoreCategories> storeCategories;
  final List<NuvidioDepartment> nuvideoDepartments;
  final List<String> photos;

  MallStoreModel({
    required this.storeId,
    required this.mallId,
    required this.title,
    this.zipzId,
    this.logo,
    this.complement,
    this.place,
    this.phone,
    this.email,
    this.whatsappNumber,
    this.link,
    this.storeCategories = const [],
    this.photos = const [],
    this.nuvideoDepartments = const [],
    this.hasNuvideo = false,
    this.useWhatsapp = false,
  });

  MallStoreModel.fromJson(Map<String, dynamic> json)
      : storeId = json['storeId'],
        zipzId = json['zipzId'],
        title = capitalizeFirstLetters(json['title']),
        logo = json['logo'],
        complement = json['complement'],
        place = json['place'],
        phone = json['phone'],
        email = json['email'],
        link = json['dynamicLink'],
        whatsappNumber = json['whatsappNumber'],
        useWhatsapp = json['useWhatsapp'] ?? false,
        mallId = json['mallId'],
        hasNuvideo = json['hasNuvideo'] ?? false,
        nuvideoDepartments = (json['storeVideoDepartments'] ?? [])
            .map<NuvidioDepartment>((v) => NuvidioDepartment.fromJson(v))
            .toList(),
        storeCategories = (json['storeCategories'] ?? [])
            .map<StoreCategories>((v) => StoreCategories.fromJson(v))
            .toList(),
        photos = (json['photos'] ?? []).map<String>((v) => v["path"].toString()).toList();

  String? get category => storeCategories.isNotEmpty ? storeCategories.first.category : null;
}

class ZipzMallStoreModel extends MallStoreModel {
  int? nPublicOffers = 0;
  int? nPrivateOffers = 0;

  int get nOffers => nPublicOffers! + nPrivateOffers!;

  ZipzMallStoreModel.fromShortJson(Map json, String _mallId)
      : nPublicOffers = json['publicOffers'],
        nPrivateOffers = json['privateOffers'],
        super(
          zipzId: json['zipzId'],
          storeId: json['storeId'],
          mallId: _mallId,
          title: json['title'],
          complement: json['category'],
          logo: json['logo'],
          photos: [json['logo']],
          useWhatsapp: false,
          storeCategories: [StoreCategories.fromZipz(json['category'])],
        );

  ZipzMallStoreModel.fromJson(Map json, String _mallId)
      : super(
          zipzId: json['uuid'],
          storeId: json['uuid'],
          mallId: _mallId,
          title: json['name'],
          complement: json['description'],
          logo: json['image'],
          photos: [json['image']],
          useWhatsapp: false,
          storeCategories: [StoreCategories.fromZipz((json['category'] ?? {})['name'])],
        );

  ZipzMallStoreModel.fromStoreJson(Map json, String? _mallId)
      : super.fromJson(json as Map<String, dynamic>..putIfAbsent("mallId", () => _mallId));
}

class NuvidioDepartment {
  final String id, code;
  final String? name;

  NuvidioDepartment.fromJson(Map<String, dynamic> json)
      : id = json['storeVideoDepartmentId'],
        code = json['value'],
        name = json['store'];
}

class StoreCategories {
  String? categoryId, category, sectionId;
  bool? main;

  StoreCategories({required this.categoryId, this.category, this.sectionId, this.main = false});

  StoreCategories.fromZipz(this.category)
      : categoryId = "zipz",
        sectionId = "descontos",
        main = true;

  StoreCategories.fromJson(Map<String, dynamic> json)
      : categoryId = json['categoryId'],
        category = json['category'],
        sectionId = json['categoryObject']['sectionId'],
        main = json['main'];
}
