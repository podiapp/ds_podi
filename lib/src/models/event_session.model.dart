class EventSessionModel {
  String? eventSessionId;
  String? eventId;
  String? sessionDate;
  String? sessionTime;
  bool? isAllDayLong;
  String? denomination;
  int? availableSpaces;
  bool? hasLimit;
  late List<SessionTicket> tickets = [], eventTicketSessions = [];

  EventSessionModel({
    this.eventSessionId,
    this.eventId,
    this.sessionDate,
    this.sessionTime,
    this.isAllDayLong,
    this.denomination = "",
    this.availableSpaces,
    this.hasLimit = true,
  });

  EventSessionModel.fromJson(Map<String, dynamic> json) {
    eventSessionId = json['eventSessionId'];
    eventId = json['eventId'];
    sessionDate = json['sessionDate'];
    sessionTime = json['sessionTime'];
    isAllDayLong = json['isAllDayLong'];
    denomination = json['denomination'] ?? "";
    availableSpaces = json['availableSpaces'];
    hasLimit = json['hasLimit'];
    tickets = (json['tickets'] as List? ?? [])
        .map<SessionTicket>((json) => SessionTicket.fromJson(json))
        .toList();
    eventTicketSessions = (json['eventTicketSessions'] as List? ?? [])
        .map<SessionTicket>((json) => SessionTicket.fromJson(json))
        .toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['eventSessionId'] = this.eventSessionId;
    data['eventId'] = this.eventId;
    data['sessionDate'] = this.sessionDate;
    data['sessionTime'] = this.sessionTime;
    data['isAllDayLong'] = this.isAllDayLong;
    data['denomination'] = this.denomination;
    data['availableSpaces'] = this.availableSpaces;
    data['hasLimit'] = this.hasLimit;
    data['tickets'] = this.tickets.map<Map<String, dynamic>>((t) => t.toJson()).toList();
    data['eventTicketSessions'] =
        this.tickets.map<Map<String, dynamic>>((t) => t.toJson()).toList();
    return data;
  }
}

class SessionTicket {
  String? eventTicketSessionId, eventTicketId, name;
  int? limit, limitPerUser, limitPerDay;
  num? price;
  bool? active, hasLimit, hasLimitPerUser, hasLimitPerDay;

  SessionTicket({
    this.eventTicketSessionId,
    this.eventTicketId,
    this.name,
    this.limit,
    this.limitPerDay,
    this.limitPerUser,
    this.hasLimit = false,
    this.hasLimitPerUser = false,
    this.hasLimitPerDay = false,
    this.price,
    this.active,
  });

  SessionTicket.fromJson(Map<String, dynamic> json)
      : eventTicketSessionId = json['eventTicketSessionId'],
        eventTicketId = json['eventTicketId'],
        name = json['name'],
        hasLimit = (json['limit'] ?? 0) < 99999,
        hasLimitPerUser = json['hasLimitPerUser'] ?? false,
        hasLimitPerDay = json['hasLimitPerDay'] ?? false,
        limit = json['limit'] ?? 0,
        limitPerDay = json['maxLimitPerDay'] ?? 0,
        limitPerUser = json['maxLimitPerUser'] ?? 0,
        price = json['price'] ?? 0,
        active = json['active'] ?? false;

  Map<String, dynamic> toJson() => {
        "eventTicketSessionId": eventTicketSessionId,
        "eventTicketId": eventTicketId,
        "name": name,
        "limit": limit,
        "maxLimitPerDay": limitPerDay,
        "maxLimitPerUser": limitPerUser,
        "price": price,
        "active": active
      };
}
