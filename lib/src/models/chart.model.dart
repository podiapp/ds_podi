import 'package:flutter/material.dart';

/// Modelo base para os valores dos gráficos
/// * `String` title;
/// * `int` quantity;
/// * `Color?` color;
class ChartDataModel {
  String title;
  dynamic quantity;
  String? icon, whiteIcon;
  int? otherValue;
  Color? color;

  ChartDataModel(this.title, this.quantity,
      {this.icon, this.whiteIcon, this.otherValue, this.color});

  @override
  String toString() => "$title: $quantity";
}

/// Modelo base para as tabelas de lojas em destaque
/// * `String` storeId;
/// * `String` storeName;
/// * `int` chat;
/// * `int` videoChat;
/// * `int` favorite;
/// * `int` hits;
class StoresDataModel {
  String? storeId, storeName;
  int? chat, videoChat, favorite, hits, position;

  StoresDataModel(this.storeId, this.storeName, this.chat, this.videoChat, this.favorite, this.hits,
      this.position);

  StoresDataModel.fromJson(Map<String, dynamic> json)
      : storeId = json['storeId'],
        storeName = json['name'],
        chat = json['chat'],
        videoChat = json['videoChat'],
        favorite = json['favorite'],
        hits = json['hits'],
        position = json['position'];
}
