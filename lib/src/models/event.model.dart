import 'dart:math';

import 'package:intl/intl.dart';

import '../utils.dart';
import 'payment_methods/payment_method.model.dart';

enum EventType { NOVIDADES, EVENTO }

extension EventTypeExtension on EventType {
  String get name {
    switch (this) {
      case EventType.NOVIDADES:
        return "Novidade";
      case EventType.EVENTO:
        return "Evento";
    }
  }
}

class EventModel {
  String? eventId, title, mallId, mallName, userId;
  String? logo,
      info,
      phone,
      email,
      webUrl,
      dateStart,
      createdAt,
      dateEnd,
      local,
      paymentMessage;
  DateTime dateStartAsDate;
  DateTime? dateEndAsDate;
  int? subscriptionNumber, status;
  EventType type;
  int tickets, checkins, enrolled;
  bool hasTickets,
      onlyGuests,
      isForMinors,
      requiredResponsible,
      needName,
      needCPF,
      needBirthdate,
      needEmail,
      needPhone,
      needProfession,
      needMaritalStatus,
      needAddress;
  List<String>? photos = [];
  List<EventSession>? sessions = [];

  EventModel({
    this.eventId,
    this.title,
    this.mallId,
    this.mallName,
    this.userId,
    this.logo,
    this.info,
    this.paymentMessage,
    this.phone,
    this.email,
    this.webUrl,
    this.dateStart,
    this.createdAt,
    this.dateEnd,
    this.local,
    this.subscriptionNumber,
    this.status,
    this.type = EventType.NOVIDADES,
    this.hasTickets = false,
    this.onlyGuests = false,
    this.isForMinors = false,
    this.requiredResponsible = false,
    this.needName = false,
    this.needCPF = false,
    this.needBirthdate = false,
    this.needEmail = false,
    this.needProfession = false,
    this.needPhone = false,
    this.needAddress = false,
    this.needMaritalStatus = false,
    this.photos,
    this.sessions,
    this.tickets = 0,
    this.checkins = 0,
    this.enrolled = 0,
    this.dateEndAsDate,
    required this.dateStartAsDate,
  });

  EventModel.fromJson(Map<String, dynamic> json)
      : type = EventType.values[json['type']],
        dateStartAsDate = getDate(json['dateStartAsDate']) ?? DateTime.now(),
        dateEndAsDate = getDate(json['dateEndAsDate']),
        photos = ((json['photos'] ?? []) as List)
            .map<String>((v) => v['path'])
            .toList(),
        sessions = (json['eventSessions'] as List? ?? [])
            .map<EventSession>((v) => EventSession.fromJson(v))
            .toList(),
        info = json['info'],
        paymentMessage = json['paymentMessage'],
        webUrl = json['webUrl'],
        logo = json['logo'],
        title = json['title'],
        mallId = json['mallId'],
        mallName = json['mallName'],
        eventId = json['eventId'],
        phone = json['phone'],
        hasTickets = json['hasTickets'] ?? false,
        onlyGuests = json['onlyGuests'] ?? false,
        needName = (json['requiredInput'] as Map? ?? {})['name'] ?? false,
        needCPF = (json['requiredInput'] as Map? ?? {})['cpf'] ?? false,
        needBirthdate =
            (json['requiredInput'] as Map? ?? {})['birthdate'] ?? false,
        needEmail = (json['requiredInput'] as Map? ?? {})['email'] ?? false,
        needPhone = (json['requiredInput'] as Map? ?? {})['phone'] ?? false,
        needAddress = (json['requiredInput'] as Map? ?? {})['address'] ?? false,
        needMaritalStatus =
            (json['requiredInput'] as Map? ?? {})['maritalStatus'] ?? false,
        needProfession =
            (json['requiredInput'] as Map? ?? {})['profession'] ?? false,
        isForMinors = json['isForMinors'] ?? false,
        requiredResponsible = json['requiredResponsible'] ?? false,
        email = json['email'],
        local = json['local'],
        subscriptionNumber = json['subscriptionNumber'],
        status = json['status'],
        createdAt = json['createdAt'],
        dateStart = json['dateStart'],
        tickets = json['tickets'] ?? 0,
        checkins = json['checkins'] ?? 0,
        enrolled = json['enrolled'] ?? 0,
        dateEnd = json['dateEnd'];

  List<DateTime>? get dates {
    final result = sessions?.map<DateTime>((s) => s.date).toSet().toList();
    result?.sort();
    return result;
  }
}

class EventSession {
  static final _format = DateFormat("dd/MM/yyyy");

  String id, denomination;
  DateTime date;
  String sessionTime, sessionDate;
  bool isAllDayLong, hasLimit;
  int availableSpaces;
  List<EventTicket> tickets;

  EventSession.fromJson(Map<String, dynamic> json)
      : id = json['eventSessionId'],
        sessionDate = json['sessionDate'],
        date = _format.parse(json['sessionDate']),
        sessionTime = json['sessionTime'],
        isAllDayLong = json['isAllDayLong'] ?? false,
        hasLimit = json['hasLimit'] ?? false,
        denomination = json['denomination'],
        availableSpaces = (json['availableSpaces'] as num).toInt(),
        tickets = (json['eventTicketSessions'] as List? ?? [])
            .map<EventTicket>(
                (j) => EventTicket.fromJson((json['hasLimit'] ?? false), j))
            .toList();
}

class SelectedEventTickets {
  EventModel event;
  EventSession session;
  List<EventTicket> tickets;
  TicketUser responsable = TicketUser();
  CreditCard? paymentMethod;

  SelectedEventTickets(
      {required this.tickets, required this.event, required this.session});

  int get totalTickets {
    int n = 0;
    tickets.forEach((t) => n += t.tickets);
    return n;
  }

  double get subTotal {
    var result = 0.0;
    for (var t in tickets) result += t.subTotal;
    return result;
  }

  double get total {
    return subTotal;
  }
}

class EventTicket {
  final String title, description, id;
  final double value;
  final bool hasLimitPerUser, hasLimitPerDay, hasLimit;
  final int maxLimitPerUser, limit, maxLimitPerDay;

  EventTicket.fromJson(bool hasLimit, Map<String, dynamic> json)
      : title = json['name'],
        id = json['eventTicketSessionId'],
        description = json['description'] ?? "",
        this.hasLimit = hasLimit,
        hasLimitPerUser = json['hasLimitPerUser'] ?? false,
        hasLimitPerDay = json['hasLimitPerDay'] ?? false,
        maxLimitPerUser = json['maxLimitPerUser'] ?? 0,
        maxLimitPerDay = json['maxLimitPerUser'] ?? 0,
        limit = (json['availableSpaces'] as num).toInt(),
        value = json['price'] / 100;

  int tickets = 0;
  List<TicketUser> users = [];

  bool get canAdd => (hasLimit && hasLimitPerDay && hasLimitPerUser)
      ? tickets + 1 <= min(limit, min(maxLimitPerDay, maxLimitPerUser))
      : (hasLimit)
          ? (hasLimitPerDay)
              ? tickets + 1 <= min(limit, maxLimitPerDay)
              : (hasLimitPerUser)
                  ? tickets + 1 <= min(limit, maxLimitPerUser)
                  : tickets + 1 <= limit
          : (hasLimitPerDay)
              ? tickets + 1 <= maxLimitPerDay
              : (hasLimitPerUser)
                  ? tickets + 1 <= maxLimitPerUser
                  : tickets + 1 <= 10;

  bool get canRemove => tickets >= 1;

  void add() {
    if (canAdd) {
      tickets++;
      users.add(TicketUser());
    }
  }

  void remove() {
    if (canRemove) {
      tickets--;
      users.removeLast();
    }
  }

  double get subTotal => value * tickets;
}

class TicketUser {
  String? id, name, cpf, email, phone, birthdate, address;

  TicketUser();

  @override
  String toString() {
    if (!isNull(cpf)) return "$name - $cpf";
    if (!isNull(email)) return "$name - $email";
    if (!isNull(phone)) return "$name - $phone";
    return name ?? "Usuário Anônimo";
  }
}
