/// Classe modelo para os usuários.
///
/// * `String` userId;
/// * `String` name;
/// * `String` photoUrl;
class UserModel {
  String? userId;
  String? name;
  String? photoUrl;
  String? email;
  String? phone;

  ///Construtor principal da classe `UserModel`.
  UserModel({
    this.userId,
    this.name,
    this.photoUrl,
    this.email,
    this.phone,
  });

  UserModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    name = json['name'];
    photoUrl = json['photoUrl'];
    email = json['email'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['name'] = this.name;
    data['photoUrl'] = this.photoUrl;
    data['email'] = this.email;
    data['phone'] = this.phone;
    return data;
  }

  UserModel copy() {
    return UserModel(
      userId: userId,
      name: name,
      photoUrl: photoUrl,
      email: email,
      phone: phone,
    );
  }
}
