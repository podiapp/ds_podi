import 'package:flutter/material.dart';

import '../utils.dart';
import 'mall_store.model.dart';
import 'payment_model.dart';

class TakeatOrderItem {
  final String? id, details, name;
  final double? totalPrice, unitPrice, weight;
  final int? amount;
  final bool? useWeight;

  String get title {
    if (useWeight!) return "${weight}kg $name";
    return "${amount}x $name";
  }

  TakeatOrderItem.fromJson(Map<String, dynamic> json)
      : id = json['id'].toString(),
        details = json['details'],
        useWeight = json['useWeight'],
        name = json['product']['name'],
        unitPrice = double.tryParse(json['product']['pricePromotion'] ?? json['product']['price']),
        totalPrice = double.tryParse(json['totalPrice']),
        weight = double.tryParse(json['weight']),
        amount = json['amount'];
}

class TakeatPaymentTicketModel extends PaymentModel {
  final String? storeId, passwordNumber;
  final double? totalPriceWithoutSms, totalPriceWithSms, discount;
  final List<TakeatOrderItem> orderItems;
  final bool hasSms;
  final OrderStatus orderStatus;
  double get smsPrice => hasSms ? totalPriceWithSms! - totalPriceWithoutSms! : 0.0;

  TakeatPaymentTicketModel.fromJson(Map<String, dynamic> json)
      : storeId = json['storeId'],
        orderItems = (json['takeAtOrder']['orders'] as List? ?? [])
            .map<TakeatOrderItem>((j) => TakeatOrderItem.fromJson(j))
            .toList(),
        orderStatus = OrderStatus.values[["pending", "accepted", "done", "finished"]
            .indexOf(json['takeAtOrder']['orderStatus'])],
        hasSms = json['takeAtOrder']['willReceiveSms'] ?? false,
        discount = double.tryParse(json['takeAtOrder']['discount'] ?? "") ?? 0,
        totalPriceWithSms = double.tryParse(json['takeAtOrder']['totalPriceWithSms']),
        totalPriceWithoutSms = double.tryParse(json['takeAtOrder']['totalPrice']),
        passwordNumber =
            (json['takeAtOrder']['attendancePassword'] ?? {})['passwordNumber'].toString(),
        super(
          createdAt: getDate(json['takeAtOrder']['createdAt']) ?? DateTime.now(),
          externalId: json['storeOrderId'],
          mallId: json['mallId'],
          paymentDate: getDate(json['takeAtOrder']['paymentApprovedDate']),
          title: "Pagamento praça de alimentação",
          type: PaymentType.GASTRONOMY,
          userId: json['userId'],
          value: double.tryParse(
            json['takeAtOrder']['totalPriceWithSms'] ?? json['takeAtOrder']['totalPrice'],
          ),
        ) {
    final _status = json['takeAtOrder']['paymentStatus'];
    if (_status == "pending")
      status = PaymentStatus.Processando;
    else if (_status == "expired")
      status = PaymentStatus.Negado;
    else if (_status == "paid")
      status = PaymentStatus.Efetivado;
    else
      status = PaymentStatus.Completo;
  }
  static final _orderStatus = [OrderStatus.Done, OrderStatus.Accepted, OrderStatus.Pending];
  static final _paymentStatus = [PaymentStatus.Negado];
  @override
  Color get statusBackgroundColor {
    if (_orderStatus.contains(orderStatus) && !_paymentStatus.contains(status))
      return orderStatus.backgroundColor;
    return status.backgroundColor;
  }

  @override
  Color get statusColor {
    if (_orderStatus.contains(orderStatus) && !_paymentStatus.contains(status))
      return orderStatus.color;
    return status.color;
  }

  @override
  String get statusText {
    if (_orderStatus.contains(orderStatus) && !_paymentStatus.contains(status))
      return orderStatus.text;
    return status.text;
  }

  MallStoreModel? store;
}
