import 'package:intl/intl.dart';

class GreenpassTicketModel {
  final String? parkingName, parkingAddress, ticketNumber, ticketToken;
  final DateTime? entrance, exit;
  final double? amount, paidAmount;
  final int? amountInCents;

  GreenpassTicketModel(
      {this.parkingName,
      this.parkingAddress,
      this.ticketNumber,
      this.ticketToken,
      this.entrance,
      this.exit,
      this.amount,
      this.paidAmount,
      this.amountInCents});

  GreenpassTicketModel.fromJson(this.ticketNumber, Map<String, dynamic> json)
      : parkingName = json["parkingName"],
        parkingAddress = json["parkingAddress"],
        amount = json["amount"],
        paidAmount = json["paidAmount"],
        amountInCents = json["amountInCents"],
        entrance = DateFormat("dd/MM/yyyy HH:mm").parse(json['entranceDate']['formattedDate']),
        exit = DateFormat("dd/MM/yyyy HH:mm").parse(json['exitDate']['formattedDate']),
        ticketToken = json["ticketToken"];
}
