class EventsTicketSessionsModel {
  String? eventTicketSessionId;
  String? eventTicketId;
  String? eventSessionId;
  String? sessionDate;
  String? session;
  String? spaces;
  bool? active;

  EventsTicketSessionsModel(
      {this.eventTicketSessionId,
      this.eventTicketId,
      this.eventSessionId,
      this.sessionDate,
      this.session,
      this.spaces,
      this.active});

  EventsTicketSessionsModel.fromJson(Map<String, dynamic> json) {
    eventTicketSessionId = json['eventTicketSessionId'];
    eventTicketId = json['eventTicketId'];
    eventSessionId = json['eventSessionId'];
    sessionDate = json['sessionDate'];
    session = json['session'];
    spaces = json['spaces'];
    active = json['active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['eventTicketSessionId'] = this.eventTicketSessionId;
    data['eventTicketId'] = this.eventTicketId;
    data['eventSessionId'] = this.eventSessionId;
    data['sessionDate'] = this.sessionDate;
    data['session'] = this.session;
    data['spaces'] = this.spaces;
    data['active'] = this.active;
    return data;
  }
}
