import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../utils.dart';

/// Classe que possui todos os filtros de serviços do PODI (Descontos, Zipz, entre outros)
class BigFilter {
  final String id, title, description, parameter;

  BigFilter({
    required this.title,
    required this.description,
    required this.parameter,
  }) : id = parameter;

  void toggleOn(Map<String, dynamic> filter) =>
      filter.update(parameter, (_) => true, ifAbsent: () => true);

  void toggleOff(Map<String, dynamic> filter) => filter.remove(parameter);
}

class BigFilterBanner {
  final String? title, subtitle, description, icon;
  final List<String> filtersIds;
  final Color backgroundColor;
  final bool hasImagePadding;
  final Size imageSize;
  bool seen = false;

  BigFilterBanner(
      {required this.title,
      this.subtitle,
      this.imageSize = const Size.square(100),
      this.hasImagePadding = true,
      required this.description,
      required this.icon,
      required this.filtersIds,
      this.backgroundColor = PodiColors.green});

  Widget build(void Function() onUpdate) {
    return Container(
      width: double.infinity,
      color: backgroundColor,
      child: IntrinsicHeight(
        child: Row(
          // crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16).copyWith(right: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title!,
                      style: PodiTexts.title
                          .copyWith(color: PodiColors.white, fontWeight: FontWeight.w700),
                    ),
                    if (!isNull(subtitle))
                      Text(
                        subtitle!,
                        style: PodiTexts.caption
                            .copyWith(color: PodiColors.white, fontWeight: FontWeight.w700),
                      ),
                    SizedBox(height: 8),
                    Text(description!, style: PodiTexts.caption.copyWith(color: PodiColors.white)),
                    SizedBox(height: 16),
                    GestureDetector(
                      onTap: () {
                        seen = true;
                        onUpdate();
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: PodiColors.white),
                          borderRadius: BorderRadius.circular((6)),
                          color: Colors.transparent,
                        ),
                        height: 24,
                        width: 58,
                        alignment: Alignment.center,
                        child: Text(
                          "Entendi",
                          style: PodiTexts.theme(
                            fontSize: 10,
                            color: PodiColors.white,
                            letterSpacing: 0.2,
                            height: 1.1,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              alignment: hasImagePadding ? Alignment.center : Alignment.bottomRight,
              padding: hasImagePadding ? EdgeInsets.all(16).copyWith(left: 0) : EdgeInsets.zero,
              child: icon!.endsWith(".svg")
                  ? SvgPicture.asset(
                      icon!,
                      height: imageSize.height,
                      width: imageSize.width,
                    )
                  : Image.asset(
                      icon!,
                      height: imageSize.height,
                      width: imageSize.width,
                    ),
            )
          ],
        ),
      ),
    );
  }
}
