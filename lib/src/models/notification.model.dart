import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../utils.dart';

enum NotificationType {
  Store,
  Gastronomy,
  Services,
  Events,
  Facilities,
  Parking,
  Discount,
  Cinema,
  Cupon,
  Misc
}

extension NotificationTypeExtension on NotificationType {
  String get icon {
    switch (this) {
      case NotificationType.Store:
        return PodiIcons.store;
      case NotificationType.Gastronomy:
        return PodiIcons.gastronomy;
      case NotificationType.Services:
        return PodiIcons.servicos;
      case NotificationType.Events:
        return PodiIcons.events;
      case NotificationType.Facilities:
        return PodiIcons.babyCart;
      case NotificationType.Parking:
        return PodiIcons.car;
      case NotificationType.Discount:
        return PodiIcons.discount;
      case NotificationType.Cinema:
        return PodiIcons.cinema;
      case NotificationType.Cupon:
        return PodiIcons.cupom;
      default:
        return PodiIcons.notify;
    }
  }

  Color get color {
    switch (this) {
      case NotificationType.Store:
        return PodiColors.purple;
      case NotificationType.Gastronomy:
        return Color(0xFF36B37E);
      case NotificationType.Services:
        return Color(0xFFFFAB00);
      case NotificationType.Events:
        return Color(0xFF00E3D6);
      case NotificationType.Facilities:
        return Color(0xFF6554C0);
      case NotificationType.Parking:
        return Color(0xFF00a8ff);
      case NotificationType.Discount:
        return Color(0xFFff5630);
      case NotificationType.Cinema:
        return Color(0xFFff79d9);
      case NotificationType.Cupon:
        return PodiColors.green;
      default:
        return PodiColors.green;
    }
  }
}

String? _searchForText(Map json) {
  final pt = json["pt"];
  if (pt != null) return pt;

  final en = json["en"];
  if (en != null) return en;
  return json[json.keys.first];
}

class NotificationModel {
  final String? id, notificationId, text, url;
  DateTime? date, readAt;
  final OneSignalNotification oneSignalNotification;
  final NotificationType type;

  String? get icon => type.icon;
  Color? get backgroundColor => type.color;
  bool get seen => readAt != null && DateTime.now().isAfter(readAt!);

  NotificationModel.fromJson(Map json)
      : id = json['id'],
        notificationId = json['notification']['notificationId'],
        oneSignalNotification = OneSignalNotification.fromJson(json["oneSignalNotification"] ?? {}),
        text = json['notification']['title'],
        url = json['notification']['url'],
        type = NotificationType.values[(json['notification']['notificationIconType'] as int?) ?? 9],
        readAt = json['readAt'] != null ? getDate(json['readAt']) : null,
        date = json['receivedAt'] != null ? getDate(json['receivedAt']) : DateTime.now();

  NotificationModel.fromOneSignalJson(Map json)
      : id = json['id'],
        notificationId = json["id"],
        oneSignalNotification = OneSignalNotification.fromJson(json["oneSignalNotification"] ?? {}),
        text = _searchForText((json['oneSignalNotification'] ?? {})['contents']),
        url = (json['oneSignalNotification'] ?? {})['appUrl'],
        type = NotificationType.Misc,
        readAt = json['readAt'] != null ? getDate(json['readAt']) : null,
        date = json['receivedAt'] != null ? getDate(json['receivedAt']) : DateTime.now();
}

class OneSignalNotification {
  final String? appId, id, appUrl;
  OneSignalNotification.fromJson(Map<String, dynamic> json)
      : appId = json["appId"],
        id = json["oneSignalNotificationId"],
        appUrl = json["appUrl"];
}
