part of "../payment_model.dart";

enum PaymentStatus {
  Processando,
  Efetivado,
  Negado,
  Aguardando,
  Completo,
}

extension PaymentStatusExtension on PaymentStatus {
  Color get backgroundColor {
    switch (this) {
      case PaymentStatus.Negado:
        return PodiColors.danger[100]!;
      case PaymentStatus.Efetivado:
        return PodiColors.green[100]!;
      case PaymentStatus.Completo:
        return PodiColors.green[100]!;
      case PaymentStatus.Processando:
        return PodiColors.warning[100]!;
      case PaymentStatus.Aguardando:
        return PodiColors.dark[100]!;
    }
  }

  Color get color {
    switch (this) {
      case PaymentStatus.Negado:
        return PodiColors.danger;
      case PaymentStatus.Efetivado:
        return PodiColors.green;
      case PaymentStatus.Completo:
        return PodiColors.green;
      case PaymentStatus.Processando:
        return PodiColors.warning;
      case PaymentStatus.Aguardando:
        return PodiColors.dark[300]!;
    }
  }

  String get text {
    switch (this) {
      case PaymentStatus.Negado:
        return "Negado";
      case PaymentStatus.Efetivado:
        return "Pago";
      case PaymentStatus.Completo:
        return "Pago";
      case PaymentStatus.Processando:
        return "Processando";
      case PaymentStatus.Aguardando:
        return "Aguardando";
    }
  }
}

enum OrderStatus { Pending, Accepted, Done, Finished }

extension OrderStatusExtension on OrderStatus {
  Color get backgroundColor {
    switch (this) {
      case OrderStatus.Pending:
        return PodiColors.warning[100]!;
      case OrderStatus.Accepted:
        return PodiColors.blue.withOpacity(0.1);
      case OrderStatus.Done:
        return PodiColors.green[100]!;
      case OrderStatus.Finished:
        return PodiColors.green[100]!;
    }
  }

  Color get color {
    switch (this) {
      case OrderStatus.Pending:
        return PodiColors.warning;
      case OrderStatus.Accepted:
        return PodiColors.blue;
      case OrderStatus.Done:
        return PodiColors.green;
      case OrderStatus.Finished:
        return PodiColors.green;
    }
  }

  String get text {
    switch (this) {
      case OrderStatus.Pending:
        return "Aguardando";
      case OrderStatus.Accepted:
        return "Em Preparo";
      case OrderStatus.Done:
        return "Pronto";
      case OrderStatus.Finished:
        return "Pago";
    }
  }
}
