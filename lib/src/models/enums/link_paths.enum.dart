enum LinkPath {
  Evento,
  Cupom,
  Loja,
  Aviso,
  Filme,
  Produto,
  Pagina,
  Navegacao,
  Banner,
  Campanha,
  Notificacao,
  Clube,
  CampanhaClube,
  EventoPagina,
  CupomPagina,
  LojaPagina,
  FilmePagina,
  ProdutoPagina,
}

extension PathExtension on LinkPath {
  String get text {
    switch (this) {
      case LinkPath.Evento:
        return "Evento";
      case LinkPath.Cupom:
        return "Desconto";
      case LinkPath.Loja:
        return "Loja";
      case LinkPath.Aviso:
        return "Aviso";
      case LinkPath.Banner:
        return "Banner";
      case LinkPath.Campanha:
        return "Campanha Externa";
      case LinkPath.Filme:
        return "Cinema";
      case LinkPath.Produto:
        return "Produto";
      case LinkPath.Pagina:
        return "Página";
      case LinkPath.Navegacao:
        return "Navegação";
      case LinkPath.Notificacao:
        return "Notificacao";
      case LinkPath.Clube:
        return "Mini Campanha PODI";
      case LinkPath.CampanhaClube:
        return "Campanha PODI";
      case LinkPath.EventoPagina:
        return "Eventos";
      case LinkPath.CupomPagina:
        return "Descontos";
      case LinkPath.LojaPagina:
        return "Lojas";
      case LinkPath.FilmePagina:
        return "Filmes";
      case LinkPath.ProdutoPagina:
        return "Produtos";
    }
  }

  String get page {
    switch (this) {
      case LinkPath.Evento:
        return "/shopping/event/detail";
      case LinkPath.Cupom:
        return "/shopping/discounts/coupon-details";
      case LinkPath.Loja:
        return "/default-detail";
      case LinkPath.Aviso:
        return "";
      case LinkPath.Banner:
        return "";
      case LinkPath.Campanha:
        return "/shopping/campaign";
      case LinkPath.Filme:
        return "/shopping/movie/detail";
      case LinkPath.Produto:
        return "/shopping/vitrine";
      case LinkPath.Pagina:
        return "";
      case LinkPath.Navegacao:
        return "";
      case LinkPath.Notificacao:
        return "";
      case LinkPath.Clube:
        return "/shopping/clube-podi";
      case LinkPath.CampanhaClube:
        return "/shopping/clube-podi/new-campaign";
      case LinkPath.EventoPagina:
        return "/shopping/events";
      case LinkPath.CupomPagina:
        return "/shopping/discounts";
      case LinkPath.LojaPagina:
        return "/shopping/stores";
      case LinkPath.FilmePagina:
        return "/shopping/movies";
      case LinkPath.ProdutoPagina:
        return "/shopping/vitrine/detail";
    }
  }
}
