enum CouponType { Product, General, Gift }

const Map<CouponType, String> kCouponTypeToString = {
  CouponType.Product: "Produto",
  CouponType.General: "Geral",
  CouponType.Gift: "Brinde"
};
