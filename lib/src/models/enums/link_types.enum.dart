enum LinkType { Interno, Externo, Whatsapp, None }

extension TypeExtension on LinkType {
  String get text {
    switch (this) {
      case LinkType.Interno:
        return "Link Interno";
      case LinkType.Externo:
        return "Link Externo";
      case LinkType.Whatsapp:
        return "Whatsapp";
      case LinkType.None:
        return "Não inserir link";
    }
  }
}
