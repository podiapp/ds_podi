enum EventTicketType { FREE, PAID }

const Map<EventTicketType, String> kEventTicketTypeToString = {
  EventTicketType.FREE: "Grátis",
  EventTicketType.PAID: "Pago"
};
