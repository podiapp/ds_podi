enum StoreType {
  STORE("Lojas"),
  FOOD_COURT("Gastronomia");

  final String text;
  const StoreType(this.text);
}

const Map<StoreType, String> kStoreTypeToString = {
  StoreType.STORE: "STORE",
  StoreType.FOOD_COURT: "FOOD_COURT",
};

const Map<String, StoreType> kStringToStoreType = {
  "STORE": StoreType.STORE,
  "FOOD_COURT": StoreType.FOOD_COURT,
};
