enum MaritalStatus { Single, Married, Widowed, Divorced, CommonLawMarriage }

Map<MaritalStatus, String> kMaritalStatusToString = {
  MaritalStatus.CommonLawMarriage: "União Estável",
  MaritalStatus.Divorced: "Divorciado(a)",
  MaritalStatus.Married: "Casado(a)",
  MaritalStatus.Single: "Solteiro(a)",
  MaritalStatus.Widowed: "Viúvo(a)",
};
