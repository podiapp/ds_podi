enum Education {
  EnsinoFundamentalIncompleto,
  EnsinoFundamentalCompleto,
  EnsinoMedioIncompleto,
  EnsinoMedioCompleto,
  EnsinoSuperiorIncompleto,
  EnsinoSuperiorCompleto,
}

const Map<Education, String> kEducationToString = {
  Education.EnsinoFundamentalIncompleto: "Ensino Fundamental Incompleto",
  Education.EnsinoFundamentalCompleto: "Ensino Fundamental Completo",
  Education.EnsinoMedioIncompleto: "Ensino Médio Incompleto",
  Education.EnsinoMedioCompleto: "Ensino Médio Completo",
  Education.EnsinoSuperiorIncompleto: "Ensino Superior Incompleto",
  Education.EnsinoSuperiorCompleto: "Ensino Superior Completo",
};

const Map<String, Education> kStringToEducation = {
  "Ensino Fundamental Incompleto": Education.EnsinoFundamentalIncompleto,
  "Ensino Fundamental Completo": Education.EnsinoFundamentalCompleto,
  "Ensino Médio Incompleto": Education.EnsinoMedioIncompleto,
  "Ensino Médio Completo": Education.EnsinoMedioCompleto,
  "Ensino Superior Incompleto": Education.EnsinoSuperiorIncompleto,
  "Ensino Superior Completo": Education.EnsinoSuperiorCompleto,
};
