enum DateType { today, yesterday, week, month, sixtyDays, ninetyDays, year, personalized }

extension DateTypeExtension on DateType {
  String get text {
    switch (this) {
      case DateType.today:
        return 'Hoje';
      case DateType.yesterday:
        return 'Ontem';
      case DateType.week:
        return 'Esta semana';
      case DateType.month:
        return 'Este mês';
      case DateType.sixtyDays:
        return "Últimos 60 dias";
      case DateType.ninetyDays:
        return "Últimos 90 dias";
      case DateType.year:
        return "Este ano";
      case DateType.personalized:
        return "Personalizado";
    }
  }
}
