enum Genre { Masculine, Feminine, Others }

const Map<Genre, String> kGenreToChar = {
  Genre.Masculine: "M",
  Genre.Feminine: "F",
  Genre.Others: "O",
};

const Map<String, Genre> kCharToGenre = {
  "M": Genre.Masculine,
  "F": Genre.Feminine,
  "O": Genre.Others,
};

extension GenreExtension on Genre {
  String get title {
    switch (this) {
      case Genre.Masculine:
        return "Masculino";
      case Genre.Feminine:
        return "Feminino";
      case Genre.Others:
        return "Outros";
    }
  }
}
