class SocialUserModel {
  String? email;
  String? name;
  String? token;
  String? type;
  String? ipAddress;

  SocialUserModel({this.email, this.name, this.token});

  SocialUserModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    name = json['name'];
    token = json['token'];
    type = json['type'];
    ipAddress = json['ipAddress'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['name'] = this.name;
    data['token'] = this.token;
    data['type'] = this.type;
    data['ipAddress'] = this.ipAddress;
    return data;
  }
}
