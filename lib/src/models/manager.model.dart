
class ManagerModel {
  String? userId;
  String? name;
  String? email;
  String? fusionUserId;
  String? accessProfileId;
  String? accessProfileName;
  List<UserMallModel>? userMalls;
  String? password;

  ManagerModel({
    this.userId,
    this.name,
    this.email,
    this.fusionUserId,
    this.accessProfileId,
    this.accessProfileName,
    this.userMalls,
    this.password,
  });

  ManagerModel.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    name = json['name'];
    email = json['email'];
    fusionUserId = json['fusionUserId'];
    accessProfileId = json['accessProfileId'];
    accessProfileName = json['accessProfileName'];
    userMalls = <UserMallModel>[];
    json['userMalls']
        .forEach((v) => userMalls?.add(new UserMallModel.fromJson(v)));
  }
}

class UserMallModel {
  String? userMallId;
  String? userId;
  String? mallId;
  String? name;

  UserMallModel({this.userMallId, this.userId, this.mallId, this.name});

  UserMallModel.fromJson(Map<String, dynamic> json) {
    userMallId = json['userMallId'];
    userId = json['userId'];
    mallId = json['mallId'];
    name = json['name'];
  }
}
