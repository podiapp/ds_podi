import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../utils.dart';

/// Classe modelo para os avisos.
///
/// * `String` id;
/// * `String` title;
/// * `String` description;
/// * `String` imageUrl;
/// * `String` redirect;
/// * `bool` imageOnTop;
/// * `DateTime` sendAfter;
/// * `DateTime` endDate;
/// * `AnnouncementAction` actions;
class AnnouncementModel {
  String? id, title, description, imageUrl, redirect, mallId, status;
  bool? imageOnTop;
  DateTime? sendAfter, endDate;
  late AnnouncementAction actions;

  AnnouncementModel(this.id, this.title, this.description, this.imageUrl,
      this.redirect, this.imageOnTop, this.sendAfter, this.endDate)
      : actions = AnnouncementAction.confirm();

  AnnouncementModel.empty() {
    this.id = "";
    this.title = "";
    this.mallId = "";
    this.description = "";
    this.imageUrl = "";
    this.redirect = "";
    this.status = "";
    this.imageOnTop = false;
    this.sendAfter = DateTime.now();
    this.endDate = DateTime.now();
    this.actions = AnnouncementAction.confirm();
  }

  AnnouncementModel.fromJson(Map json) {
    this.id = json['noticeId'];
    this.title = json['title'];
    this.mallId = json['mallId'];
    this.description = json['description'];
    this.imageUrl = json['photoUrl'];
    this.redirect = json['url'];
    this.status = json['status'];
    this.imageOnTop = (json['noticeType'] ?? {})['title'] == "Campanha";
    this.sendAfter = getDate(json['sendAfter']);
    this.endDate = getDate(json['endDate']);
    this.actions = isNull(json['url'])
        ? AnnouncementAction.exit()
        : AnnouncementAction.confirm();
  }
}

class NoticeType {
  String? noticeTypeId, title, notice;
  DateTime? createdAt, updatedAt;
  bool? active;

  NoticeType.fromJson(Map<String, dynamic> json)
      : this.noticeTypeId = json['noticeTypeId'],
        this.title = json['title'],
        this.notice = json['notice'],
        this.createdAt = getDate(json['createdAt']),
        this.updatedAt = getDate(json['updatedAt']),
        this.active = json['active'];

  Map<String, dynamic> toJson() {
    return {
      'noticeTypeId': noticeTypeId,
      'title': title,
      'notice': notice,
      'createdAt': DateFormat("yyyy-MM-dd'T'HH:mm:ss").format(createdAt!),
      'updatedAt': DateFormat("yyyy-MM-dd'T'HH:mm:ss").format(DateTime.now()),
      'active': active
    };
  }
}

/// Classe para as ações dos avisos
///
/// * `List<Widget>` widgets;
/// * `AnnouncementActionType` type;
/// * `bool` redirectOnConfirm;
class AnnouncementAction {
  final List<Widget> widgets;
  final AnnouncementActionType type;
  final bool redirectOnConfirm;

  AnnouncementAction.exit([String text = "FECHAR"])
      : widgets = [
          Container(
            padding: EdgeInsets.all(16),
            child: Text(text.toUpperCase(), style: PodiTexts.button1),
          )
        ],
        redirectOnConfirm = false,
        type = AnnouncementActionType.Exit;

  AnnouncementAction.confirm(
      {String laterText = "DEPOIS",
      String acceptText = "VER MAIS",
      this.redirectOnConfirm = false})
      : widgets = [
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: PodiColors.dark[500]!),
            ),
            alignment: Alignment.center,
            child: Text(
              laterText.toUpperCase(),
              style: PodiTexts.theme(
                fontWeight: FontWeight.w600,
                color: PodiColors.dark[500],
                letterSpacing: 0.5,
                fontSize: 12,
                height: 1.35,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: PodiColors.green,
            ),
            alignment: Alignment.center,
            child: Text(
              acceptText.toUpperCase(),
              style: PodiTexts.theme(
                fontWeight: FontWeight.w600,
                color: PodiColors.white,
                letterSpacing: 0.5,
                fontSize: 12,
                height: 1.35,
              ),
            ),
          )
        ],
        type = AnnouncementActionType.Confirm;
}

/// [Enum] dos tipos de ações dos avisos.
///
/// * Exit
/// * Confirm
enum AnnouncementActionType { Exit, Confirm }
