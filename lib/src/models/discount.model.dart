import 'mall_store.model.dart';

class DiscountItemModel {
  final String? id, zipzId, title, subtitle, description, url;
  final double? oldPrice, newPrice;
  final String? redeemTime, quantity, status;
  MallStoreModel? store;
  final bool public;

  bool get soldOut => quantity == "sold out";

  String get discount => "${((oldPrice! - newPrice!) / oldPrice! * 100).toStringAsFixed(0)}% OFF";

  DiscountItemModel.fromZipzJson(Map<String, dynamic> json, this.store, [this.public = true])
      : title = json["name"],
        id = json["uuid"],
        zipzId = json['uuid'],
        subtitle = json['category'] != null ? json["category"][0]["name"] : "",
        description = json["description"],
        quantity = json["quantity"],
        status = json["status"],
        url = json["image"],
        oldPrice = double.tryParse(json["fullPrice"]),
        newPrice = double.tryParse(json["offerPrice"]),
        redeemTime = json["expirationPeriod"].toString();

  DiscountItemModel.fromPodiJsonAndStore(Map<String, dynamic> json, this.store,
      [this.public = true])
      : title = json["name"],
        id = json["offerId"],
        zipzId = json['zipzId'],
        subtitle = json['categories'] != null ? json["categories"][0]["name"] : "",
        description = json["description"],
        quantity = json["quantity"],
        status = json["status"],
        url = json["image"],
        oldPrice = double.tryParse(json["fullPrice"]),
        newPrice = double.tryParse(json["offerPrice"]),
        redeemTime = json["expirationPeriod"].toString();
}
