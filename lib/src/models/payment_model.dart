import '../utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

part 'enums/payment_status.enum.dart';

enum PaymentType { GASTRONOMY, EVENTS, PARKING_LOT, MOVIES, OTHERS }

extension PaymentTypeExtension on PaymentType? {
  Color get color {
    switch (this!) {
      case PaymentType.GASTRONOMY:
        return Color(0xFF36B37E);
      case PaymentType.EVENTS:
        return Color(0xFF00E3D6);
      case PaymentType.PARKING_LOT:
        return Color(0xFF00a8ff);
      case PaymentType.MOVIES:
        return Color(0xFFFF79D9);
      case PaymentType.OTHERS:
        return PodiColors.green;
    }
  }

  String get icon {
    switch (this!) {
      case PaymentType.GASTRONOMY:
        return PodiIcons.gastronomy;
      case PaymentType.EVENTS:
        return PodiIcons.events;
      case PaymentType.PARKING_LOT:
        return PodiIcons.car;
      case PaymentType.MOVIES:
        return PodiIcons.cinema;
      case PaymentType.OTHERS:
        return PodiIcons.extract;
    }
  }

  String get name {
    switch (this!) {
      case PaymentType.GASTRONOMY:
        return "Gastronomia";
      case PaymentType.EVENTS:
        return "Eventos ";
      case PaymentType.PARKING_LOT:
        return "Estacionamento";
      case PaymentType.MOVIES:
        return "Filmes";
      case PaymentType.OTHERS:
        return "Outros";
    }
  }
}

class PaymentModel {
  final String? paymentId, userId, mallId, externalId;
  final String? mallName, title;
  final DateTime? paymentDate, createdAt;
  final double? value;
  final PaymentType? type;
  PaymentStatus status;

  Color get statusBackgroundColor => status.backgroundColor;
  Color get statusColor => status.color;
  String get statusText => status.text;

  PaymentModel(
      {this.paymentId,
      this.userId,
      this.mallId,
      this.externalId,
      this.createdAt,
      this.mallName,
      this.title,
      this.paymentDate,
      this.value,
      this.type,
      this.status = PaymentStatus.Aguardando});

  PaymentModel.fromJson(Map<String, dynamic> json)
      : paymentId = json['paymentId'],
        userId = json['userId'],
        mallId = json['mallId'],
        externalId = json['externalId'],
        mallName = json['mallName'],
        title = json['title'],
        paymentDate = getDate(json['paymentDate']),
        createdAt = getDate(json['createdAt']) ?? DateTime.now(),
        value = (double.tryParse(json['value'].toString()) ?? 0) / 100,
        type = PaymentType.values[json['type'] ?? 4],
        status = PaymentStatus.values[json['status'] ?? 3];

  String? get id => externalId;
}

class ParkingPaymentTicketModel extends PaymentModel {
  final String? ticketNumber, nsu, ticketToken, cardToken;
  final DateTime? entrance, exit;
  final bool? chargeCancelled;

  ParkingPaymentTicketModel.failed(
      {this.ticketNumber, this.entrance, double? price, String? id, String? mallId})
      : nsu = null,
        ticketToken = null,
        cardToken = null,
        exit = null,
        chargeCancelled = true,
        super(
          value: price,
          externalId: id,
          mallId: mallId,
          createdAt: DateTime.now(),
          type: PaymentType.PARKING_LOT,
          status: PaymentStatus.Negado,
        );

  ParkingPaymentTicketModel.fromJson(Map<String, dynamic> json)
      : cardToken = json['cardToken'],
        ticketNumber = json['parkingLotTicketId'],
        nsu = json['nsu'],
        ticketToken = json['ticketToken'],
        entrance = getDate(json['entranceDate']),
        exit = getDate(json['maxExitDate']),
        chargeCancelled = json['chargeCancelled'],
        super(
            type: PaymentType.PARKING_LOT,
            externalId: json['ticketId'],
            userId: json['userId'],
            mallId: json['mallId'],
            createdAt: getDate(json['createdAt']),
            paymentId: json['paymentId'],
            mallName: json['mallName'],
            status: PaymentStatus.values[json['ticketStatus'] ?? 0],
            value: (double.tryParse(json['amount'].toString()) ?? 0) / 100,
            paymentDate: getDate(json['paymentDate']),
            title: "Ticket Estacionamento");
}

class EventPaymentTicketModel extends PaymentModel {
  final int registrationNumber;
  final List<EventPaymentTicketUser> users;

  EventPaymentTicketModel.fromJson(Map<String, dynamic> json, PaymentModel payment)
      : registrationNumber = json['registrationNumber'],
        users = (json['eventTicketPayUsers'] as List? ?? [])
            .map((j) => EventPaymentTicketUser.fromJson(j))
            .toList(),
        super(
          type: PaymentType.EVENTS,
          externalId: json['eventTicketPayId'],
          userId: json['userId'],
          mallId: payment.mallId,
          createdAt: getDate(json['reservedAt']),
          paymentId: payment.paymentId,
          mallName: payment.mallName,
          status: payment.status,
          value: (double.tryParse(json['totalAmount'].toString()) ?? 0) / 100,
          paymentDate: getDate(json['boughtAt']),
          title: "Pagamento de evento",
        );

  EventPaymentTicketModel.fromPayments(EventPaymentTicketModel event, PaymentModel payment)
      : registrationNumber = event.registrationNumber,
        users = event.users,
        super(
          type: PaymentType.EVENTS,
          externalId: event.externalId,
          userId: event.userId,
          mallId: payment.mallId,
          createdAt: event.createdAt,
          paymentId: payment.paymentId,
          mallName: payment.mallName,
          status: payment.status,
          value: payment.value,
          paymentDate: payment.paymentDate,
          title: event.title,
        );
}

class EventPaymentTicketUser {
  final String eventTicketPayUserId, eventTicketSessionId, eventTicketPayId;
  final String? name, cpf, birthdate, address, email, phone;
  final double amount;
  final DateTime? reservedAt, boughtAt;
  EventPaymentTicketUser.fromJson(Map<String, dynamic> json)
      : eventTicketPayUserId = json['eventTicketPayUserId'],
        eventTicketSessionId = json['eventTicketSessionId'],
        eventTicketPayId = json['eventTicketPayId'],
        name = json['name'],
        cpf = json['cpf'],
        birthdate = json['birthdate'],
        address = json['address'],
        email = json['email'],
        phone = json['phone'],
        amount = json['amount'] / 100,
        reservedAt = getDate(json['reservedAt']),
        boughtAt = getDate(json['boughtAt']);

  @override
  String toString() {
    if (!isNull(cpf)) return "$name - $cpf";
    if (!isNull(email)) return "$name - $email";
    if (!isNull(phone)) return "$name - $phone";
    return name ?? "Usuário Anônimo";
  }
}
