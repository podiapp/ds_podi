import 'store_section.model.dart';

class StoreCategoryModel {
  final String? id, title;
  final bool? active;
  final StoreSectionModel section;

  StoreCategoryModel.fromJson(Map<String, dynamic> json)
      : id = json['categoryId'],
        title = json['title'],
        section = StoreSectionModel.fromJson(json['section']),
        active = json['active'];

  StoreCategoryModel.empty()
      : id = "",
        title = "Sem categoria",
        section = StoreSectionModel.empty(),
        active = false;
}
