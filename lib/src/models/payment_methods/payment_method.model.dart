import '../../utils.dart';

export 'credit_card.model.dart';

class PaymentMethod {
  final String? token, type;

  PaymentMethod(this.token, this.type);

  String? get icon => PodiIcons.creditCard;

  String? get subtitle => null;
}

class CreditInDelivery extends PaymentMethod {
  CreditInDelivery() : super("-1", "Cartão de Crédito");

  @override
  String get subtitle => "Pagamento na entrega";
}

class DebitInDelivery extends PaymentMethod {
  DebitInDelivery() : super("-2", "Cartão de Débito");

  @override
  String get subtitle => "Pagamento na entrega";
}
