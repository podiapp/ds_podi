import '../../utils.dart';

import 'payment_method.model.dart';

class CreditCard extends PaymentMethod {
  bool? isMain, active;

  String? cardId, lastDigits, cardBrand, userId, createdAt, updatedAt;

  CreditCard.fromJson(Map<String, dynamic> json)
      : cardId = json["cardId"],
        lastDigits = json["lastDigits"],
        cardBrand = json["cardBrand"],
        userId = json["userId"],
        createdAt = json["createdAt"],
        updatedAt = json["updatedAt"],
        isMain = json['isMain'],
        active = json['active'],
        super(json['cardToken'], capitalizeFirstLetters(json["cardBrand"]));

  Map<String, dynamic> toJson() => {
        "cardToken": token,
        "cardId": cardId,
        "lastDigits": lastDigits,
        "cardBrand": cardBrand,
        "isMain": isMain,
        "userId": userId,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
        "active": active,
      };

  @override
  String? get icon => kStringToBrand[cardBrand?.toUpperCase()];

  @override
  String get subtitle => "Final: $lastDigits";
}

class InputCreditCard {
  bool isMain = true;
  String? number, cvv, name, brand, expirationDate;
}

class CardRequestModel {
  String? cardNumber, holderName, expDate, securityCode, userId;
  bool? isMain;

  CardRequestModel.fromCreditCard(InputCreditCard card, this.userId)
      : cardNumber = card.number,
        holderName = card.name,
        expDate = card.expirationDate,
        securityCode = card.cvv,
        isMain = card.isMain;

  CardRequestModel.fromJson(Map<String, dynamic> json)
      : cardNumber = json["cardNumber"],
        holderName = json["holderName"],
        expDate = json["expDate"],
        securityCode = json["securityCode"],
        userId = json["userId"],
        isMain = json["isMain"];
}

const kStringToBrand = {
  "AURA": PodiIcons.brandAura,
  "VISA": PodiIcons.brandVisa,
  "AMEX": PodiIcons.brandAmex,
  "DISCOVER": PodiIcons.brandDiscover,
  "MASTERCARD": PodiIcons.brandMastercard,
  "DINERS CLUB": PodiIcons.brandDiners,
  "JCB": PodiIcons.brandJcb,
  "ELO": PodiIcons.brandElo,
  "HIPER": PodiIcons.brandHiper,
  "HIPERCARD": PodiIcons.brandHipercard,
  "RUPAY": PodiIcons.brandRupay,
};

const kBrandToString = {
  PodiIcons.brandAura: "Aura",
  PodiIcons.brandVisa: "Visa",
  PodiIcons.brandAmex: "Amex",
  PodiIcons.brandDiscover: "Discover",
  PodiIcons.brandMastercard: "Mastercard",
  PodiIcons.brandDiners: "Diners Club",
  PodiIcons.brandJcb: "JCB",
  PodiIcons.brandElo: "Elo",
  PodiIcons.brandHiper: "Hiper",
  PodiIcons.brandHipercard: "Hipercard",
  PodiIcons.brandRupay: "Rupay",
};
