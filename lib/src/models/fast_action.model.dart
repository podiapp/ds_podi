import 'dart:convert';

import '../utils.dart';
import 'package:flutter/material.dart';

class AppNavigations {
  final String? title, icon, url, id, container;
  final int? order;
  final Map<String, dynamic>? arguments;

  AppNavigations(
      {this.title, this.icon, this.url, this.id, this.order, this.arguments, this.container});
  AppNavigations.fromJson(Map<String, dynamic> _json)
      : title = _json['title'],
        icon = _json['icon'],
        url = _json['url'],
        id = _json['appNavigationId'],
        container = _json['appContainerId'],
        order = _json['order'],
        arguments = _json['arguments'] is List && _json['arguments'].isNotEmpty
            ? json.decode(_json['arguments'][0]['value'] ?? "{}")
            : null;

  Widget build(BuildContext context) => Container();
}

class FastActionModel extends AppNavigations {
  final Color color;

  FastActionModel.fromJson(Map<String, dynamic> json)
      : color = colorFromHex(json['color']),
        super.fromJson(json);
}
