class EventTicketGridModel {
  String? eventTicketId;
  String? type;
  String? salesPeriod;
  String? sold;
  int? price;
  int? limitPerUser;
  int? limitPerDay;
  bool? active;
  bool? hasLimitPerDay;
  bool? hasLimitPerUser;
  bool? canEdit;

  EventTicketGridModel({
    this.eventTicketId,
    this.salesPeriod,
    this.type,
    this.price,
    this.sold,
    this.limitPerUser,
    this.limitPerDay,
    this.active,
    this.hasLimitPerDay,
    this.hasLimitPerUser,
    this.canEdit,
  });

  EventTicketGridModel.fromJson(Map<String, dynamic> json) {
    eventTicketId = json['eventTicketId'];
    type = json['type'];
    salesPeriod = json['salesPeriod'];
    price = json['price'];
    sold = json['sold'];
    limitPerDay = json['limitPerDay'];
    limitPerUser = json['limitPerUser'];
    active = json['active'];
    hasLimitPerDay = json['hasLimitPerDay'];
    hasLimitPerUser = json['hasLimitPerUser'];
    canEdit = json['canEdit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['eventTicketId'] = this.eventTicketId;
    data['type'] = this.type;
    data['salesPeriod'] = this.salesPeriod;
    data['limitPerUser'] = this.limitPerUser;
    data['limitPerDay'] = this.limitPerDay;
    data['price'] = this.price;
    data['sold'] = this.sold;
    data['active'] = this.active;
    data['hasLimitPerDay'] = this.hasLimitPerDay;
    data['hasLimitPerUser'] = this.hasLimitPerUser;
    data['canEdit'] = this.canEdit;
    return data;
  }
}
