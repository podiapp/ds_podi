import 'enums/enums.dart';

class StoreSectionModel {
  String? sectionId;
  String? title;
  StoreType? type;
  String? icon;
  String? photoUrl;
  String? createdAt;
  String? updatedAt;
  bool? active;

  StoreSectionModel(
      {this.sectionId,
      this.title,
      this.type,
      this.icon,
      this.photoUrl,
      this.createdAt,
      this.updatedAt,
      this.active});

  StoreSectionModel.fromJson(Map<String, dynamic> json) {
    sectionId = json['sectionId'];
    title = json['title'];
    type = StoreType.values[json['type']];
    icon = json['icon'];
    photoUrl = json['photoUrl'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    active = json['active'];
  }

  StoreSectionModel.empty() {
    active = false;
    createdAt = "";
    icon = "";
    photoUrl = "";
    sectionId = "";
    title = "";
    type = StoreType.STORE;
    updatedAt = "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sectionId'] = this.sectionId;
    data['title'] = this.title;
    data['type'] = this.type;
    data['icon'] = this.icon;
    data['photoUrl'] = this.photoUrl;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['active'] = this.active;
    return data;
  }
}
