import 'enums/enums.dart';

import 'store_category.model.dart';

import '../utils.dart';

class CategorySection {
  final String? title, icon, id, categoryId, photoUrl;
  final StoreType type;

  CategorySection.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        icon = json['icon'],
        photoUrl = json['photoUrl'] ?? "",
        id = json['section']['sectionId'],
        categoryId = json['categoryId'],
        type = StoreType.values[json['type']];
}

class CategorySectionOrganizer {
  final String? title, id;
  List<StoreCategoryModel> categories;

  CategorySectionOrganizer({this.title = "", this.id = "", this.categories = const []}) {
    categories.sort((a, b) {
      if (a.title!.toUpperCase() == "OUTROS") return 1000;
      if (b.title!.toUpperCase() == "OUTROS") return -1000;

      return removeDiacritics(a.title!).compareTo(removeDiacritics(b.title!));
    });
  }
}
