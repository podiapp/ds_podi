import '../utils.dart';

class CampaignModel {
  String? id, mallId, title, info, terms, image, url;
  bool active;
  DateTime? startDate, endDate, createdAt;

  CampaignModel(
      {this.id,
      this.mallId,
      this.title,
      this.info,
      this.url,
      this.active = false,
      this.terms,
      this.image,
      this.endDate,
      this.startDate,
      this.createdAt});

  CampaignModel.fromJson(Map<String, dynamic> json)
      : id = json['campaignId'],
        mallId = json["mallId"],
        title = json["title"],
        info = json["info"],
        terms = json["terms"],
        url = json["url"],
        active = json["active"],
        image = json["image"],
        startDate = getDate(json['startDate']),
        endDate = getDate(json['endDate']),
        createdAt = getDate(json['createdAt']);
}
