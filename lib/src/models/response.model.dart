import 'package:dio/dio.dart';

///Modelo para organização das respostas da API
///
///Abre segundo o modelo da API do PODI:
///```json
///{
///    "message": "",
///    "success": true,
///    "data": ...
///}
///```
class ResponseModel {
  final dynamic data;
  final Response response;
  final int? statusCode;
  final String? message, statusMessage;
  final bool success;

  static bool _getSuccess(int? statusCode) {
    return statusCode?.toString().startsWith("2") ?? false;
  }

  static T? _getValue<T>(Response response, String parameter, [T? defaultValue]) {
    if (response.data is Map) return response.data[parameter] ?? defaultValue;
    return defaultValue;
  }

  ///Construtor principal que recebe um objeto do [Response] do dio
  ResponseModel(this.response)
      : this.statusCode = response.statusCode,
        this.statusMessage = response.statusMessage,
        this.message = _getValue<String?>(response, 'message'),
        this.success = _getValue<bool>(response, 'success', _getSuccess(response.statusCode))!,
        this.data = _getValue(response, 'data');
}
