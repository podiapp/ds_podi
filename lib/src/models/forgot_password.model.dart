class ForgotPassword {
  String? message;
  int? statusCode;
  Data? data;

  ForgotPassword({this.message, this.statusCode, this.data});

  ForgotPassword.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    statusCode = json['statusCode'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['statusCode'] = this.statusCode;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? changePasswordId;
  String? email;

  Data({this.changePasswordId, this.email});

  Data.fromJson(Map<String, dynamic> json) {
    changePasswordId = json['changePasswordId'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['changePasswordId'] = this.changePasswordId;
    data['email'] = this.email;
    return data;
  }
}
