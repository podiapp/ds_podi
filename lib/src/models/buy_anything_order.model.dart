import 'dart:io';

import 'mall_store.model.dart';

class BuyAnythingOrderModel {
  final String? details;
  final double? value;
  final List<File> images;
  final MallStoreModel? store;

  BuyAnythingOrderModel({this.details = "", this.value = 0, this.images = const [], this.store});
}
