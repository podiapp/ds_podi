import '../utils.dart';

class MallBannersModel {
  String? path, link, mallId, mallBannerId;
  int? type, order;
  bool active;
  DateTime? createdAt;

  void Function()? onTap;

  MallBannersModel({
    this.path,
    this.link,
    this.mallId,
    this.mallBannerId,
    this.type,
    this.order,
    this.active = false,
    this.createdAt,
    this.onTap,
  });

  MallBannersModel.fromJson(Map<String, dynamic> json)
      : path = json['path'],
        link = json['link'],
        mallId = json['mallId'],
        mallBannerId = json['mallBannerId'],
        type = json['type'],
        order = json['order'],
        active = json['active'] ?? false,
        createdAt = getDate(json['createdAt']);
}
