class EventSessionGridModel {
  String? eventSessionId;
  String? sessionDate;
  String? session;
  String? spaces;
  bool? canEdit;
  bool? hasLimit;

  EventSessionGridModel(
      {this.eventSessionId,
      this.sessionDate,
      this.session,
      this.spaces,
      this.canEdit,
      this.hasLimit = true});

  EventSessionGridModel.fromJson(Map<String, dynamic> json) {
    eventSessionId = json['eventSessionId'];
    sessionDate = json['sessionDate'];
    session = json['session'];
    spaces = json['spaces'];
    canEdit = json['canEdit'];
    hasLimit = json['hasLimit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['eventSessionId'] = this.eventSessionId;
    data['sessionDate'] = this.sessionDate;
    data['session'] = this.session;
    data['spaces'] = this.spaces;
    data['canEdit'] = this.canEdit;
    data['hasLimit'] = this.hasLimit;
    return data;
  }
}
