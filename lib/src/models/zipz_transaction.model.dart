import 'mall_store.model.dart';
import '../utils.dart';

import 'discount.model.dart';

class ZipzTransactionModel {
  ZipzMallStoreModel? store;
  DiscountItemModel? offer;
  String? couponId, transactionId, redemptionCode, status, staff, qrCard;
  DateTime? expirationTime, redeemedAt;

  ZipzTransactionModel.fromJson(Map<String, dynamic> json, String? mallId) {
    store = ZipzMallStoreModel.fromStoreJson(json['store'], mallId);
    offer = DiscountItemModel.fromPodiJsonAndStore(json['offer'], store);
    couponId = json['couponId'];
    transactionId = json['transactionId'];
    staff = json['staff'];
    qrCard = (json['qrCard'] ?? {})['name'];
    status = json['status'];
    if (json['expirationTime'] != null)
      expirationTime = getDate(json['expirationTime']);
    if (json['redeemedAt'] != null)
      redeemedAt = getDate(json['redeemedAt']);
    else if (status == "redeemed") redeemedAt = DateTime.now();
    redemptionCode = json['redemptionCode'];
  }

  bool get redeemed => redeemedAt != null || status == "redeemed";

  bool get expired => !redeemed && expirationTime!.isBefore(DateTime.now());
}
