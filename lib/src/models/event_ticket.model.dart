import 'enums/enums.dart';
import '../utils.dart';

class EventTicketModel {
  String? eventTicketSessionId;
  String? eventTicketId;
  String? eventId;
  EventTicketType? type;
  String? name;
  double? limit;
  double? price;
  DateTime? startDate;
  DateTime? endDate;
  String? startDateString;
  String? endDateString;
  int? minLimitPerUser;
  int? maxLimitPerUser;
  int? minLimitPerDay;
  int? maxLimitPerDay;
  bool? hasLimitPerUser;
  bool? hasLimitPerDay;
  bool? active;
  String? description;

  EventTicketModel({
    this.eventTicketSessionId,
    this.eventTicketId,
    this.eventId,
    this.type = EventTicketType.FREE,
    this.name,
    this.limit = 0,
    this.price = 0,
    this.startDate,
    this.endDate,
    this.minLimitPerUser = 1,
    this.maxLimitPerUser = 1,
    this.minLimitPerDay = 1,
    this.maxLimitPerDay = 1,
    this.hasLimitPerUser = false,
    this.hasLimitPerDay = false,
    this.active = false,
    this.description,
  });

  EventTicketModel.fromJson(Map<String, dynamic> json) {
    eventTicketSessionId = "";
    eventTicketId = json['eventTicketId'];
    eventId = json['eventId'];
    type = EventTicketType.values[json['type']];
    name = json['name'];
    limit = json['limit'];
    price = json['price'];
    startDate = getDate(json['startDate']);
    endDate = getDate(json['endDate']);
    startDateString = json['startDate'];
    endDateString = json['endDate'];
    minLimitPerUser = json['minLimitPerUser'];
    maxLimitPerUser = json['maxLimitPerUser'];
    minLimitPerDay = json['minLimitPerDay'];
    maxLimitPerDay = json['maxLimitPerDay'];
    hasLimitPerUser = json['hasLimitPerUser'];
    hasLimitPerDay = json['hasLimitPerDay'];
    active = json['active'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['eventTicketId'] = this.eventTicketId;
    data['eventId'] = this.eventId;
    data['type'] = this.type;
    data['name'] = this.name;
    data['limit'] = this.limit;
    data['price'] = this.price;
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    data['minLimitPerUser'] = this.minLimitPerUser;
    data['maxLimitPerUser'] = this.maxLimitPerUser;
    data['minLimitPerDay'] = this.minLimitPerDay;
    data['maxLimitPerDay'] = this.maxLimitPerDay;
    data['hasLimitPerDay'] = this.hasLimitPerDay;
    data['hasLimitPerUser'] = this.hasLimitPerUser;
    data['active'] = this.active;
    data['description'] = this.description;
    return data;
  }
}
