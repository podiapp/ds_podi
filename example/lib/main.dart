import 'package:ds_podi/ds_podi.dart';
import 'package:example/pages/charts_page.dart';
import 'package:example/pages/components_page.dart';
import 'package:example/pages/data_table_page.dart';
import 'package:example/pages/styles_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ds_podi',
      theme: podiThemeData(context),
      home: const MyHomePage(title: 'Widget testing'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Tab> tabs = [
    const Tab(text: "Styles"),
    const Tab(text: "Charts"),
    const Tab(text: "Components"),
    const Tab(text: "Data Table"),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      initialIndex: 1,
      child: Scaffold(
        backgroundColor: PodiColors.white,
        appBar: AppBar(
          title: Text(widget.title,
              style: PodiTexts.body1.withColor(PodiColors.white)),
          backgroundColor: PodiColors.green,
          bottom: TabBar(
            isScrollable: true,
            indicatorColor: PodiColors.white,
            labelColor: PodiColors.white,
            labelStyle: PodiTexts.button1,
            tabs: tabs,
          ),
        ),
        body: const TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            StylesPage(),
            ChartsPage(),
            ComponentsPage(),
            DataTablePage(),
          ],
        ),
      ),
    );
  }
}
