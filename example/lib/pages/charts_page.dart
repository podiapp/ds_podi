import 'dart:math';

import 'package:ds_podi/ds_podi.dart';
import 'package:flutter/material.dart';

class ChartsPage extends StatelessWidget {
  const ChartsPage({Key? key}) : super(key: key);

  Widget buildContainer({required String title, required Widget child}) {
    return Column(
      children: [
        Text(title),
        const SizedBox(height: 16),
        Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            color: PodiColors.white,
            border: Border.all(color: PodiColors.dark[100]!),
            borderRadius: BorderRadius.circular(16),
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, 8),
                blurRadius: 24,
                color: PodiColors.dark.withOpacity(0.04),
              ),
            ],
          ),
          child: child,
        ),
      ],
    );
  }

  PodiCharts get chart {
    const names = "ABCDE";
    List<PodiChartData<String>> data =
        List<PodiChartData<String>>.generate(2, (index) {
      return PodiChartData<String>(
        x: names[index],
        y: (Random().nextDouble() * 100).toInt() + 1,
        group: List<PodiChartData<String>>.generate(names.length, (index) {
          return PodiChartData<String>(
            x: names[index],
            y: (Random().nextDouble() * 100).toInt() + 1,
          );
        }),
      );
    });

    final style = PodiChartStyle(enableDataLabel: true, enableLegend: true);

    return PodiCharts<String>(data: data, style: style);
  }

  Widget _buildFirst() {
    return Row(
      children: <Widget>[
        Expanded(
          child: buildContainer(
            title: "Donut Chart",
            child: chart.donut,
          ),
        ),
        Expanded(
          child: buildContainer(
            title: "Pie Chart",
            child: chart.pie,
          ),
        ),
      ].separatedBy(const SizedBox(width: 16)),
    );
  }

  Widget _buildSecond() {
    return Row(
      children: <Widget>[
        Expanded(
          child: buildContainer(
            title: "Bar Chart",
            child: chart.bar,
          ),
        ),
        Expanded(
          child: buildContainer(
            title: "GroupedBar Chart",
            child: chart.groupedBar,
          ),
        ),
      ].separatedBy(const SizedBox(width: 16)),
    );
  }

  Widget _buildThird() {
    return Row(
      children: <Widget>[
        Expanded(
          child: buildContainer(
            title: "Line Chart",
            child: chart.line,
          ),
        ),
        Expanded(
          child: buildContainer(
            title: "Area Chart",
            child: chart.area,
          ),
        ),
      ].separatedBy(const SizedBox(width: 16)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(24),
      child: Column(
        children: <Widget>[
          _buildFirst(),
          _buildSecond(),
          _buildThird(),
        ].separatedBy(const SizedBox(height: 16)),
      ),
    );
  }
}
