import 'package:ds_podi/ds_podi.dart';
import 'package:flutter/material.dart';

class StylesPage extends StatelessWidget {
  const StylesPage({Key? key}) : super(key: key);

  Widget get _texts {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("H1/Regular", style: PodiTexts.heading1.weightRegular),
        Text("H1/Medium", style: PodiTexts.heading1),
        Text("H1/Semibold", style: PodiTexts.heading1.weightSemibold),
        Text("H1/Bold", style: PodiTexts.heading1.weightBold),
        const SizedBox(height: 48),
        Text("H2/Regular", style: PodiTexts.heading2.weightRegular),
        Text("H2/Medium", style: PodiTexts.heading2),
        Text("H2/Semibold", style: PodiTexts.heading2.weightSemibold),
        Text("H2/Bold", style: PodiTexts.heading2.weightBold),
        const SizedBox(height: 48),
        Text("H3/Regular", style: PodiTexts.heading3.weightRegular),
        Text("H3/Medium", style: PodiTexts.heading3),
        Text("H3/Semibold", style: PodiTexts.heading3.weightSemibold),
        Text("H3/Bold", style: PodiTexts.heading3.weightBold),
        const SizedBox(height: 48),
        Text("H4/Regular", style: PodiTexts.heading4.weightRegular),
        Text("H4/Medium", style: PodiTexts.heading4),
        Text("H4/Semibold", style: PodiTexts.heading4.weightSemibold),
        Text("H4/Bold", style: PodiTexts.heading4.weightBold),
        const SizedBox(height: 48),
        Text("H5/Regular", style: PodiTexts.heading5.weightRegular),
        Text("H5/Medium", style: PodiTexts.heading5),
        Text("H5/Semibold", style: PodiTexts.heading5.weightSemibold),
        Text("H5/Bold", style: PodiTexts.heading5.weightBold),
        const SizedBox(height: 48),
        Text("H6/Regular", style: PodiTexts.heading6.weightRegular),
        Text("H6/Medium", style: PodiTexts.heading6),
        Text("H6/Semibold", style: PodiTexts.heading6.weightSemibold),
        Text("H6/Bold", style: PodiTexts.heading6.weightBold),
        const SizedBox(height: 48),
        Text("H7/Regular", style: PodiTexts.heading7.weightRegular),
        Text("H7/Medium", style: PodiTexts.heading7),
        Text("H7/Semibold", style: PodiTexts.heading7.weightSemibold),
        Text("H7/Bold", style: PodiTexts.heading7.weightBold),
        const SizedBox(height: 48),
        Text("Body 1/Regular", style: PodiTexts.body1.weightRegular),
        Text("Body 1/Medium", style: PodiTexts.body1),
        Text("Body 1/Semibold", style: PodiTexts.body1.weightSemibold),
        Text("Body 1/Bold", style: PodiTexts.body1.weightBold),
        const SizedBox(height: 48),
        Text("Body 2/Regular", style: PodiTexts.body2.weightRegular),
        Text("Body 2/Medium", style: PodiTexts.body2),
        Text("Body 2/Semibold", style: PodiTexts.body2.weightSemibold),
        Text("Body 2/Bold", style: PodiTexts.body2.weightBold),
        const SizedBox(height: 48),
        Text("Label 1/Regular", style: PodiTexts.label1.weightRegular),
        Text("Label 1/Medium", style: PodiTexts.label1),
        Text("Label 1/Semibold", style: PodiTexts.label1.weightSemibold),
        Text("Label 1/Bold", style: PodiTexts.label1.weightBold),
        const SizedBox(height: 48),
        Text("Label 2/Regular", style: PodiTexts.label2.weightRegular),
        Text("Label 2/Medium", style: PodiTexts.label2),
        Text("Label 2/Semibold", style: PodiTexts.label2.weightSemibold),
        Text("Label 2/Bold", style: PodiTexts.label2.weightBold),
        const SizedBox(height: 48),
        Text("Label 3/Regular", style: PodiTexts.label3.weightRegular),
        Text("Label 3/Medium", style: PodiTexts.label3),
        Text("Label 3/Semibold", style: PodiTexts.label3.weightSemibold),
        Text("Label 3/Bold", style: PodiTexts.label3.weightBold),
      ],
    );
  }

  Widget get _colors {
    List<int> values = [50, 100, 200, 300, 400, 500, 600, 700, 800, 900];
    List<int> feedbackValues = [600, 500, 400, 300, 200];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            SizedBox(width: 100, child: Text("Green", style: PodiTexts.title)),
            const SizedBox(width: 16),
            ...List.generate(
              values.length,
              (index) => Container(
                height: 100,
                width: 100,
                color: PodiColors.green[values[index]],
                child: Center(
                    child: Text(
                  values[index].toString(),
                  style: PodiTexts.label3.withColor(values[index] >= 500
                      ? PodiColors.white
                      : PodiColors.dark),
                )),
              ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(width: 100, child: Text("Accent", style: PodiTexts.title)),
            const SizedBox(width: 16),
            ...List.generate(
              values.length,
              (index) => Container(
                height: 100,
                width: 100,
                color: PodiColors.purple[values[index]],
                child: Center(
                    child: Text(
                  values[index].toString(),
                  style: PodiTexts.label3.withColor(values[index] >= 500
                      ? PodiColors.white
                      : PodiColors.dark),
                )),
              ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(
                width: 100, child: Text("Neutrals", style: PodiTexts.title)),
            const SizedBox(width: 16),
            ...List.generate(
              values.length,
              (index) => Container(
                height: 100,
                width: 100,
                color: PodiColors.neutrals[values[index]],
                child: Center(
                    child: Text(
                  values[index].toString(),
                  style: PodiTexts.label3.withColor(values[index] >= 500
                      ? PodiColors.white
                      : PodiColors.dark),
                )),
              ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(
                width: 100,
                child: Text("Feedback Info", style: PodiTexts.title)),
            const SizedBox(width: 16),
            ...List.generate(
              feedbackValues.length,
              (index) => Container(
                height: 100,
                width: 200,
                color: PodiColors.blue[feedbackValues[index]],
                child: Center(
                    child: Text(
                  feedbackValues[index].toString(),
                  style: PodiTexts.label3,
                )),
              ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(
                width: 100,
                child: Text("Feedback Warning", style: PodiTexts.title)),
            const SizedBox(width: 16),
            ...List.generate(
              feedbackValues.length,
              (index) => Container(
                height: 100,
                width: 200,
                color: PodiColors.warning[feedbackValues[index]],
                child: Center(
                    child: Text(
                  feedbackValues[index].toString(),
                  style: PodiTexts.label3,
                )),
              ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(
                width: 100,
                child: Text("Feedback Danger\ue800", style: PodiTexts.title)),
            const SizedBox(width: 16),
            ...List.generate(
              feedbackValues.length,
              (index) => Container(
                height: 100,
                width: 200,
                color: PodiColors.danger[feedbackValues[index]],
                child: Center(
                    child: Text(
                  feedbackValues[index].toString(),
                  style: PodiTexts.label3,
                )),
              ),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(40),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Textos
          Expanded(child: _texts),
          const SizedBox(width: 24),
          Expanded(child: _colors),
        ],
      ),
    );
  }
}
