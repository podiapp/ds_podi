import 'package:ds_podi/ds_podi.dart';
import 'package:flutter/material.dart';

class DataTablePage extends StatefulWidget {
  const DataTablePage({Key? key}) : super(key: key);

  @override
  State<DataTablePage> createState() => _DataTablePageState();
}

class _DataTablePageState extends State<DataTablePage> {
  List<UserModel> get _users => List.generate(
      10,
      (index) => UserModel(
            name: index.toString() * 2,
            userId: (index + 1).toString(),
          ));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: ReordableDataTableWidget<UserModel>(
        items: _users,
        onReorder: (start, current) {},
        columns: [
          ReordableDataTableColumn<UserModel>(
            title: "NOME",
            buildItem: (item) {
              return Text(item.name ?? "-");
            },
          ),
          ReordableDataTableColumn<UserModel>(
            title: "ID",
            buildItem: (item) {
              return Text(item.userId ?? "-");
            },
          ),
          ReordableDataTableColumn<UserModel>(
            title: "AÇÃO",
            buildItem: (item) {
              return Container(
                height: 24,
                width: 24,
                decoration: BoxDecoration(
                  color: PodiColors.white,
                  borderRadius: BorderRadius.circular(6),
                  border: Border.all(color: PodiColors.dark[100]!),
                ),
                child: TextButton(
                  style: BasicButtonStyle(
                    overlayColor: PodiColors.dark.withOpacity(0.1),
                    padding: EdgeInsets.zero,
                  ),
                  onPressed: () {},
                  child: const Icon(
                    Icons.edit_outlined,
                    color: PodiColors.dark,
                    size: 16,
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
