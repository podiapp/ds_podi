import 'dart:async';

import 'package:ds_podi/ds_podi.dart';
import 'package:flutter/material.dart';

class ComponentsPage extends StatefulWidget {
  const ComponentsPage({Key? key}) : super(key: key);

  @override
  State<ComponentsPage> createState() => _ComponentsPageState();
}

class _ComponentsPageState extends State<ComponentsPage> {
  bool busy = true;
  int step = 0;
  List<String> stepNames = [
    "Teste 1",
    "Teste 2",
    "Teste 3",
    "Teste 4",
    "Teste 5",
    "Teste 6"
  ];
  Timer? _stepTimer;

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _stepTimer?.cancel();
    super.dispose();
  }

  Future<void> changeBusy() async {
    await Future.delayed(const Duration(seconds: 3));
    if (mounted) setState(() => busy = false);
  }

  Future<void> changeStep() async {
    if (_stepTimer?.isActive ?? false) return;
    _stepTimer = Timer(const Duration(seconds: 3), () {
      if (step == (stepNames.length - 1)) {
        step = 0;
      } else {
        step += 1;
      }
    });
  }

  Widget get _table {
    return TableWidget<TesteTable>(
      deviceType: DeviceType.Desktop,
      busy: busy,
      items: [
        TesteTable(id: "1", name: "A"),
        TesteTable(id: "2", name: "B", isExpandable: true),
        TesteTable(id: "3", name: "C"),
        TesteTable(id: "4", name: "D"),
        TesteTable(id: "5", name: "E"),
        TesteTable(id: "6", name: "F"),
        TesteTable(id: "7", name: "G"),
        TesteTable(id: "8", name: "H"),
        TesteTable(id: "9", name: "I"),
      ],
      columns: [
        TableColumnWidget(
          showOnMobile: false,
          title: "ID",
          width: const FlexColumnWidgetWidth(1),
          buildItem: (context, contraints, item) => Text(item.id),
        ),
        TableColumnWidget(
          title: "NAME",
          width: const FlexColumnWidgetWidth(3),
          buildItem: (context, contraints, item) => Text(item.name),
        ),
      ],
    );
  }

  Widget get _dropzones {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: DropzoneWidget(
                hasHeader: true,
                title: "Olha só",
                onDroppedItem: (_) {},
                onRemove: (_) {},
                type: DropzoneType.csv,
                text: "Arraste o arquivo .CSV aqui",
                height: 200,
              ),
            ),
            const SizedBox(width: 24),
            Expanded(
              child: DropzoneWidget(
                hasHeader: false,
                onDroppedItem: (_) {},
                onRemove: (_) {},
                height: 200,
              ),
            ),
          ],
        ),
        const SizedBox(height: 24),
        Row(
          children: [
            Expanded(
              child: DropzoneWidget(
                hasHeader: false,
                onDroppedItem: (_) {},
                onRemove: (_) {},
                height: 200,
                isSingleImage: true,
                image:
                    "https://upload.wikimedia.org/wikipedia/commons/b/bd/Test.svg",
              ),
            ),
            const SizedBox(width: 24),
            Expanded(
              child: DropzoneWidget(
                hasHeader: false,
                onDroppedItem: (_) {},
                onRemove: (_) {},
                height: 200,
                imageList: const [
                  "https://upload.wikimedia.org/wikipedia/commons/b/bd/Test.svg",
                  "https://upload.wikimedia.org/wikipedia/commons/b/bd/Test.svg",
                  "https://upload.wikimedia.org/wikipedia/commons/b/bd/Test.svg",
                  "https://upload.wikimedia.org/wikipedia/commons/b/bd/Test.svg",
                  "https://upload.wikimedia.org/wikipedia/commons/b/bd/Test.svg",
                  "https://upload.wikimedia.org/wikipedia/commons/b/bd/Test.svg"
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget get _textFields {
    return Form(
      key: _formKey,
      child: const Wrap(
        runSpacing: 16, // Espaçamento vertical
        spacing: 16, // Espaçamento horizontal
        children: [
          // Sem ícones
          PodiTextFormField(width: 200, hint: "Hint"),
          PodiTextFormField(width: 200, hint: "Hint", label: "Label"),
          PodiTextFormField(width: 200, label: "Label", isObligatory: true),
          PodiTextFormField(width: 200, label: "Label", helper: "Help Text"),
          PodiTextFormField(width: 200, label: "Label", enabled: false),
          // Com ícones
          PodiTextFormField(
            width: 200,
            suffixIcon: PodiWebIcons.search,
          ),
          PodiTextFormField(
            width: 200,
            label: "Label",
            suffixIcon: PodiWebIcons.search,
          ),
          PodiTextFormField(
            width: 200,
            label: "Label",
            isObligatory: true,
            suffixIcon: PodiWebIcons.search,
          ),
          PodiTextFormField(
            width: 200,
            label: "Label",
            helper: "Help Text",
            suffixIcon: PodiWebIcons.search,
          ),
          PodiTextFormField(
            width: 200,
            label: "Label",
            enabled: false,
            suffixIcon: PodiWebIcons.search,
          ),
          PodiTextFormField(
            width: 200,
            preffixIcon: PodiWebIcons.search,
          ),
          PodiTextFormField(
            width: 200,
            label: "Label",
            preffixIcon: PodiWebIcons.search,
          ),
          PodiTextFormField(
            width: 200,
            label: "Label",
            isObligatory: true,
            preffixIcon: PodiWebIcons.search,
          ),
          PodiTextFormField(
            width: 200,
            label: "Label",
            helper: "Help Text",
            preffixIcon: PodiWebIcons.search,
          ),
          PodiTextFormField(
            width: 200,
            label: "Label",
            enabled: false,
            preffixIcon: PodiWebIcons.search,
          ),
        ],
      ),
    );
  }

  Widget get _buttons {
    List<Widget> buildPrimary = [
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.primary(
                title: "Login", size: ButtonSize.large, enabled: false),
            PodiButton.primary(title: "Login", size: ButtonSize.large),
            PodiButton.primary(title: "Login", size: ButtonSize.medium),
            PodiButton.primary(title: "Login", size: ButtonSize.small),
            PodiButton.primary(title: "Login", size: ButtonSize.tiny),
          ].separatedBy(const SizedBox(height: 8))),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.primary(
                icon: PodiWebIcons.user,
                title: "Login",
                size: ButtonSize.large,
                enabled: false),
            PodiButton.primary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.large,
            ),
            PodiButton.primary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.medium,
            ),
            PodiButton.primary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.small,
            ),
            PodiButton.primary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.tiny,
            ),
          ].separatedBy(const SizedBox(height: 8))),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.primary(
                icon: PodiWebIcons.user,
                title: "Login",
                size: ButtonSize.large,
                controlAffinity: ListTileControlAffinity.trailing,
                enabled: false),
            PodiButton.primary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.large,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
            PodiButton.primary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.medium,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
            PodiButton.primary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.small,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
            PodiButton.primary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.tiny,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
          ].separatedBy(const SizedBox(height: 8))),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.primary(
                icon: PodiWebIcons.user,
                size: ButtonSize.large,
                enabled: false),
            PodiButton.primary(icon: PodiWebIcons.user, size: ButtonSize.large),
            PodiButton.primary(
                icon: PodiWebIcons.user, size: ButtonSize.medium),
            PodiButton.primary(icon: PodiWebIcons.user, size: ButtonSize.small),
            PodiButton.primary(icon: PodiWebIcons.user, size: ButtonSize.tiny),
          ].separatedBy(const SizedBox(height: 8))),
    ];
    List<Widget> buildSecondary = [
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.secondary(
                title: "Login", size: ButtonSize.large, enabled: false),
            PodiButton.secondary(title: "Login", size: ButtonSize.large),
            PodiButton.secondary(title: "Login", size: ButtonSize.medium),
            PodiButton.secondary(title: "Login", size: ButtonSize.small),
            PodiButton.secondary(title: "Login", size: ButtonSize.tiny),
          ].separatedBy(const SizedBox(height: 8))),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.secondary(
                icon: PodiWebIcons.user,
                title: "Login",
                size: ButtonSize.large,
                enabled: false),
            PodiButton.secondary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.large,
            ),
            PodiButton.secondary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.medium,
            ),
            PodiButton.secondary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.small,
            ),
            PodiButton.secondary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.tiny,
            ),
          ].separatedBy(const SizedBox(height: 8))),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.secondary(
                icon: PodiWebIcons.user,
                title: "Login",
                size: ButtonSize.large,
                controlAffinity: ListTileControlAffinity.trailing,
                enabled: false),
            PodiButton.secondary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.large,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
            PodiButton.secondary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.medium,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
            PodiButton.secondary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.small,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
            PodiButton.secondary(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.tiny,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
          ].separatedBy(const SizedBox(height: 8))),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.secondary(
                icon: PodiWebIcons.user,
                size: ButtonSize.large,
                enabled: false),
            PodiButton.secondary(
                icon: PodiWebIcons.user, size: ButtonSize.large),
            PodiButton.secondary(
                icon: PodiWebIcons.user, size: ButtonSize.medium),
            PodiButton.secondary(
                icon: PodiWebIcons.user, size: ButtonSize.small),
            PodiButton.secondary(
                icon: PodiWebIcons.user, size: ButtonSize.tiny),
          ].separatedBy(const SizedBox(height: 8))),
    ];
    List<Widget> buildSimple = [
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.simple(
                title: "Login", size: ButtonSize.large, enabled: false),
            PodiButton.simple(title: "Login", size: ButtonSize.large),
            PodiButton.simple(title: "Login", size: ButtonSize.medium),
            PodiButton.simple(title: "Login", size: ButtonSize.small),
            PodiButton.simple(title: "Login", size: ButtonSize.tiny),
          ].separatedBy(const SizedBox(height: 8))),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.simple(
                icon: PodiWebIcons.user,
                title: "Login",
                size: ButtonSize.large,
                enabled: false),
            PodiButton.simple(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.large,
            ),
            PodiButton.simple(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.medium,
            ),
            PodiButton.simple(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.small,
            ),
            PodiButton.simple(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.tiny,
            ),
          ].separatedBy(const SizedBox(height: 8))),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.simple(
                icon: PodiWebIcons.user,
                title: "Login",
                size: ButtonSize.large,
                controlAffinity: ListTileControlAffinity.trailing,
                enabled: false),
            PodiButton.simple(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.large,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
            PodiButton.simple(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.medium,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
            PodiButton.simple(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.small,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
            PodiButton.simple(
              icon: PodiWebIcons.user,
              title: "Login",
              size: ButtonSize.tiny,
              controlAffinity: ListTileControlAffinity.trailing,
            ),
          ].separatedBy(const SizedBox(height: 8))),
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            PodiButton.simple(
                icon: PodiWebIcons.user,
                size: ButtonSize.large,
                enabled: false),
            PodiButton.simple(icon: PodiWebIcons.user, size: ButtonSize.large),
            PodiButton.simple(icon: PodiWebIcons.user, size: ButtonSize.medium),
            PodiButton.simple(icon: PodiWebIcons.user, size: ButtonSize.small),
            PodiButton.simple(icon: PodiWebIcons.user, size: ButtonSize.tiny),
          ].separatedBy(const SizedBox(height: 8))),
    ];

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ...buildPrimary,
        ...buildSecondary,
        ...buildSimple,
      ].separatedBy(const SizedBox(width: 8)),
    );
  }

  Widget _steps() {
    return StepsWidget(stepNames, step);
  }

  Widget title(String text) => Text(text, style: PodiTexts.heading3.weightBold);

  Widget get _imageCarousel {
    return SizedBox(
      height: 250,
      width: 300,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: const ImageCarouselWidget(
          photos: [
            "https://podi-images-dev.s3.amazonaws.com/Formulario/PODI-94bfc762-56b1-4f27-ae25-aa78d8f32842.png",
            "https://podi-images-dev.s3.amazonaws.com/Formulario/PODI-526e33c1-896c-476f-a2a2-43e9854dc559.jpeg",
          ],
          adaptativeMarkers: true,
          swipeDuration: Duration(seconds: 2),
          height: 250,
          width: 300,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _formKey.currentState?.validate();
    changeBusy();
    changeStep();
    final htmlController = HtmlEditorController();
    return SingleChildScrollView(
      padding: const EdgeInsets.all(40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          title("IMAGE CAROUSEL"),
          const SizedBox(height: 16),
          _imageCarousel,
          const SizedBox(height: 24),
          title("TEXT FIELDS"),
          const SizedBox(height: 16),
          _textFields,
          const SizedBox(height: 24),
          title("BUTTONS"),
          const SizedBox(height: 16),
          _buttons,
          title("STEPS"),
          const SizedBox(height: 16),
          _steps(),
          const SizedBox(height: 24),
          title("DROPZONES"),
          const SizedBox(height: 16),
          _dropzones,
          const SizedBox(height: 24),
          title("HTML TEXT FIELD"),
          const SizedBox(height: 16),
          HtmlTextField(
            hint: " ",
            controller: htmlController,
            height: 200,
          ),
          const SizedBox(height: 24),
          title("TABLE"),
          const SizedBox(height: 8),
          _table
        ],
      ),
    );
  }
}

class TesteTable implements ExpandableTableItem {
  String id;
  String name;
  @override
  bool isExpandable;

  TesteTable({
    required this.id,
    required this.name,
    this.isExpandable = false,
  });

  @override
  Widget get child => TableWidget<UserModel>(
        items: [
          UserModel(userId: "1", name: "A", photoUrl: ""),
          UserModel(userId: "2", name: "B", photoUrl: ""),
          UserModel(userId: "3", name: "C", photoUrl: ""),
          UserModel(userId: "4", name: "D", photoUrl: ""),
          UserModel(userId: "5", name: "E", photoUrl: ""),
          UserModel(userId: "6", name: "F", photoUrl: ""),
          UserModel(userId: "7", name: "G", photoUrl: ""),
          UserModel(userId: "8", name: "H", photoUrl: ""),
          UserModel(userId: "9", name: "I", photoUrl: ""),
        ],
        columns: [
          TableColumnWidget(
            showOnMobile: false,
            title: "ID",
            width: const FlexColumnWidgetWidth(1),
            buildItem: (context, contraints, item) => Text(item.userId!),
          ),
          TableColumnWidget(
            title: "NAME",
            width: const FlexColumnWidgetWidth(3),
            buildItem: (context, contraints, item) => Text(item.name!),
          ),
        ],
      );
}
